
export class StringFormatter {
  price(value) {
    return value.toFixed(2) + '€';
  }

  percentage(value) {
    return (value*100).toFixed() + '%';
  }

  date(timestamp) {
    return moment.unix(timestamp/1000).format('YYYY/MM/DD HH:mm:ss');
  }
}
