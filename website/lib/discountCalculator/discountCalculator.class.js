
import { Promotions } from '/imports/api/promotions/promotions.js';

export class DiscountCalculator {
  calculateDiscountedPrice(article, discount) {
    let promotions = this.articleDiscounts(article);
    if (!discount) discount = Calculator.calculateCumulativeDiscount(promotions);
    return parseFloat((article.price * (1 - discount)).toFixed(2));
  }

  articleDiscounts(article) {
    const now = new Date();
    return Promotions.find({
      articleId: article._id,
      startsAt: { $lte: now },
      endsAt: { $gte: now },
    }).fetch();
  }

  calculateCumulativeDiscount(promotions) {
    if (promotions) {
      if (promotions.constructor === Array)
        return 1-this.compute(promotions);
      else if (promotions.count())
        return 1-this.compute(promotions);
    }
    return 0;
  }

  compute(promotions) {
    let total = 1;
    promotions.forEach(promotion => {
      let discount = promotion.discount/100;
      total = total * (1-discount);
    });
    return total;
  }
}
