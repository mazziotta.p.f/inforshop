
export class SecurityEnsurer {

  userIsInRole(roles) {
    return Roles.userIsInRole(Meteor.userId(), roles, 'infoRshop');
  }

  isAllowed(roles) {
    return this.userIsInRole(roles);
  }

  isDenied(roles) {
    return !this.userIsInRole(roles);
  }

  check(roles) {
    if (this.isDenied(roles)) {
      throw new Meteor.Error(403, 'Error 403: Access is forbidden');
    }
  }

  denyIfNotConnected() {
    if(!this.isConnected())
      throw new Meteor.Error(403, 'Error 403: Forbidden to logged out user');
  }

  isConnected() {
    return Meteor.userId() ? true: false;
  }
}

Security = new SecurityEnsurer();
