
// API Collections
import '/imports/api/articles/articles.js';
import '/imports/api/articles/articleDefaults.js';
import '/imports/api/articles/articleTechnicals.js';
import '/imports/api/brands/brands.js';
import '/imports/api/categories/categories.js';
import '/imports/api/csvImport/csvImport.js';
import '/imports/api/generalConfiguration/generalConfiguration.js';
import '/imports/api/images/images.js';
import '/imports/api/salesMargins/salesMargins.js';
import '/imports/api/carts/carts.js';
import '/imports/api/inventories/inventories.js';
import '/imports/api/resupplies/resupplies.js';
import '/imports/api/promotions/promotions.js';
import '/imports/api/orders/orders.js';

// API methods
import '/imports/api/articles/articles.methods.js';
import '/imports/api/articles/articleDefaults.methods.js';
import '/imports/api/articles/articleTechnicals.methods.js';
import '/imports/api/brands/brands.methods.js';
import '/imports/api/categories/categories.methods.js';
import '/imports/api/csvImport/csvImport.methods.js';
import '/imports/api/generalConfiguration/generalConfiguration.methods.js';
import '/imports/api/salesMargins/salesMargins.methods.js';
import '/imports/api/users/users.methods.js';
import '/imports/api/inventories/inventories.methods.js';
import '/imports/api/resupplies/resupplies.methods.js';
import '/imports/api/carts/carts.methods.js';
import '/imports/api/promotions/promotions.methods.js';
import '/imports/api/orders/orders.methods.js';

// API publications
import '/imports/api/articles/articles.publications.js';
import '/imports/api/articles/articleDefaults.publications.js';
import '/imports/api/articles/articleTechnicals.publications.js';
import '/imports/api/brands/brands.publications.js';
import '/imports/api/categories/categories.publications.js';
import '/imports/api/csvImport/csvImport.publications.js';
import '/imports/api/generalConfiguration/generalConfiguration.publications.js';
import '/imports/api/images/images.publications.js';
import '/imports/api/salesMargins/salesMargins.publications.js';
import '/imports/api/users/users.publications.js';
import '/imports/api/inventories/inventories.publications.js';
import '/imports/api/resupplies/resupplies.publications.js';
import '/imports/api/carts/carts.publications.js';
import '/imports/api/promotions/promotions.publications.js';
import '/imports/api/orders/orders.publications.js';

// Fixtures (database seeds)
import '/imports/api/users/users.fixtures.js';
import '/imports/api/categories/categories.fixtures.js';
import '/imports/api/brands/brands.fixtures.js';
import '/imports/api/articles/articleTechnicals.fixtures.js';
import '/imports/api/generalConfiguration/generalConfiguration.fixtures.js';

// Server & client side routes
import '/imports/ui/frontOffice/frontOffice.routes.js';
import '/imports/ui/frontOffice/home/home.routes.js';
import '/imports/ui/frontOffice/articles/articles.routes.js';
import '/imports/ui/frontOffice/order/order.routes.js';
import '/imports/ui/backOffice/backOffice.routes.js';

// Server side config
import '/imports/startup/accounts.twitter.js';
import '/imports/startup/accounts.facebook.js';
import '/imports/startup/accounts.google.js';
