export const articleDefaultsBlueprint = {
  imageName: '',
  advisedPrice: 0,
  supplierDiscount: 0,
  restockingTime: 0,
  category: '',
  brand: '',
  modelNumber: '',
  series: '',
  color: '',
  technicals: [],
}
