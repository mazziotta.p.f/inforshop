
import './components/cart/cart.js';
import './components/account/account.js';
import './components/searchBarArticles/searchBarArticles.js';

import './frontOffice/frontOffice.js';
import './backOffice/backOffice.js';

import '/imports/startup/sAlert.config.js';
import '/imports/startup/config.js';
