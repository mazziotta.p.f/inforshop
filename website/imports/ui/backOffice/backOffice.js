
import './layout/layout.js';

import './home/home.js';
import './csvImport/csvImport.js';
import './categories/categories.js';
import './salesMargins/salesMargins.js';
import './technicals/technicals.js';
import './brands/brands.js';
import './images/images.js';
import './articles/articles.js';
import './generalConfiguration/generalConfiguration.js'
import './users/users.js';
import './inventories/inventories.js';
import './resupplies/resupplies.js';
import './orders/orders.js';

import './backOffice.controller.js';
import './backOffice.routes.js';
