
import { Images } from '/imports/api/images/images.js';

Template.ArticlePromotions.onRendered(function() {
  $('.has-datetimepicker').datetimepicker()
});

Template.BackOfficeArticleEntry.events({
  'click [data-action="make-article-available"]'(event, template) {
    Meteor.call('articles.setAvailability', this._id, event.target.checked);
  },
  'click [data-action="remove-article"]'(event, template) {
    if (!confirm('Remove ?')) return;
    Meteor.call('articles.remove', this._id);
  },
});

Template.ArticleEditorForm.events({
  'click [data-action="make-article-available"]'(event, template) {
    Meteor.call('articles.setAvailability', this.article()._id,
      event.target.checked);
  },
  'blur [data-action="update-string-property"]'(event, template) {
    Meteor.call('articles.updateStringProperty', template.data.article()._id,
      event.target.dataset.propertyName, event.target.value);
  },
  'blur [data-action="update-number-property"]'(event, template) {
    Meteor.call('articles.updateNumberProperty', template.data.article()._id,
      event.target.dataset.propertyName, parseFloat(event.target.value.replace(',', '.')));
  },
  'change select[data-action="update-string-property"]'(event, template) {
    Meteor.call('articles.updateStringProperty', template.data.article()._id,
      event.target.dataset.propertyName, event.target.value);
  },
  'change [data-action="update-image"]'(event, template) {

    const files = event.target.files;

    if (files && files[0]) {
      const upload = Images.insert({
        file: files[0],
        streams: 'dynamic',
        chunkSize: 'dynamic'
      }, false);

      upload.on('start', function () {
        template.currentUpload.set(this);
      });

      upload.on('end', function (error, file) {
        if (error) {
          sAlert.error('Error during upload: ' + error.reason);
          return;
        } else {
          Meteor.call('articles.updateStringProperty',
            template.data.article()._id, 'imageName', file.name);
          sAlert.success('Files ' + file.name + '" successfully uploaded');
        }
        template.currentUpload.set(false);
      });

      upload.start();
    }
  }
});

Template.ArticleTechnicalPropertyForm.events({
  'blur [data-action="update-technical-property"]'(event, template) {
    Meteor.call('articles.updateTechnicalProperty',
      Router.current().data().article()._id, template.data.name, event.target.value);
  },
});

Template.ArticlePromotion.events({
  'click [data-action="delete-promotion"]'(event, template) {
    if (!confirm('Remove ?')) return;
    Meteor.call('promotions.remove', this._id);
  },
});

Template.ArticlePromotions.events({
  'submit [data-action="create-promotion"]'(event, template) {
    event.preventDefault();
    const target = event.target;

    Meteor.call('promotions.create', this.article()._id,
      parseFloat(target.discount.value),
      new Date(target.startsAt.value),
      new Date(target.endsAt.value));

    target.discount.value = '';
    target.startsAt.value = '';
    target.endsAt.value = '';
  },
});
