
import { ArticlesIndex } from '/imports/api/articles/articles.js'
import { Promotions } from '/imports/api/promotions/promotions.js';

Template.ArticlesEditor.helpers({
  articlesIndex: function() {
    return ArticlesIndex;
  },
});

Template.ArticleEditorForm.onCreated(function () {
  this.currentUpload = new ReactiveVar(false);
});

Template.ArticleEditorForm.helpers({
  currentUpload() {
    return Template.instance().currentUpload.get();
  },
  hasPromotions() {
    const now = new Date();
    if (this.article()) return Promotions.find({
      articleId: this.article()._id,
      startsAt: { $lte: now },
      endsAt:   { $gte: now },
    }).count() > 0;
  },
  totalDiscount() {
    const now = new Date();
    let promotions = Promotions.find({
      articleId: this.article()._id,
      startsAt: { $lte: now },
      endsAt:   { $gte: now },
    });
    return (Calculator.calculateCumulativeDiscount(promotions) * 100).toFixed(0);
  },
  discountedPrice() {
    const now = new Date();
    let promotions = Promotions.find({
      articleId: this.article()._id,
      startsAt: { $lte: now },
      endsAt:   { $gte: now },
    });
    let discount = Calculator.calculateCumulativeDiscount(promotions);
    return (this.article().price * (1 - discount)).toFixed(2);
  },
});

Template.ArticleCategoryOption.helpers({
  isSelectedCategory() {
    let article = Router.current().data().article();
    if (article) return this._id === article.categoryId;
  },
});

Template.ArticleBrandOption.helpers({
  isSelectedBrand() {
    let article = Router.current().data().article();
    if (article) return this._id === article.brandId;
  },
});

Template.ArticlePromotion.helpers({
  isInRange() {
    const now = new Date();
    return this.startsAt <= now && this.endsAt >= now;
  },
});
