
import { CSVImportArticles } from '/imports/api/csvImport/csvImport.js';
import { Categories } from '/imports/api/categories/categories.js';
import { SalesMargins } from '/imports/api/salesMargins/salesMargins.js';
import { Brands } from '/imports/api/brands/brands.js';
import { Articles } from '/imports/api/articles/articles.js';
import { ArticleDefaults } from '/imports/api/articles/articleDefaults.js';
import { GeneralConfiguration } from '/imports/api/generalConfiguration/generalConfiguration.js';
import { ArticleTechnicals } from '/imports/api/articles/articleTechnicals.js';
import { Images } from '/imports/api/images/images.js';
import { Inventories } from '/imports/api/inventories/inventories.js';
import { articleDefaultsBlueprint } from '/imports/blueprints/articleDefaults.blueprint.js';
import { Resupplies } from '/imports/api/resupplies/resupplies.js';
import { Promotions } from '/imports/api/promotions/promotions.js';
import { Orders } from '/imports/api/orders/orders.js';

Router.route('/manage/', {
  controller: 'BackOfficeController',
  name: 'backOfficeHome',
  template: 'backOfficeHome',
  subscriptions: function() {
    Meteor.subscribe('categories');
    Meteor.subscribe('brands');
    Meteor.subscribe('articles');
    Meteor.subscribe('csvImportArticles');
    Meteor.subscribe('images');
    Meteor.subscribe('articleTechnicals');
    Meteor.subscribe('articleDefaults');
    Meteor.subscribe('salesMargins');
  },
  data: {
    categoriesCount: () => Categories.find().count(),
    brandsCount: () => Brands.find().count(),
    articlesCount: () => Articles.find().count(),
    imagesCount: () => Images.find().count(),
    csvArticlesCount: () => CSVImportArticles.find().count(),
    technicalsCount: () => ArticleTechnicals.find().count(),
    articleDefaultsCount: () => ArticleDefaults.find().count(),
    salesMarginsCount: () => SalesMargins.find().count(),
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ manager';
  },
  allowedRoles: ['employee'],
});

Router.route('/manage/articles', {
  controller: 'BackOfficeController',
  name: 'articlesEditor',
  template: 'ArticlesEditor',
  subscriptions: function() {
    Meteor.subscribe('articles');
  },
  data: {
    articles: () => Articles.find(),
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ manager > articles';
  },
  allowedRoles: ['manager', 'commercial', 'admin'],
});

Router.route('/manage/article/:_id', {
  controller: 'BackOfficeController',
  name: 'articleEditor',
  template: 'ArticleEditor',
  subscriptions: function() {
    Meteor.subscribe('articles');
    Meteor.subscribe('images');
    Meteor.subscribe('brands');
    Meteor.subscribe('categories');
    Meteor.subscribe('articlePromotions', Router.current().params._id);
  },
  data: {
    article: () => Articles.findOne({_id: Router.current().params._id}),
    image: function() {
      const article = Router.current().data().article();
      if (article) {
        return Images.findOne({name: article.imageName});
      }
    },
    categories: () => Categories.find(),
    brands: () => Brands.find(),
    promotions: () => Promotions.find({}, {sort: {startsAt: -1, endsAt: -1}}),
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ manager > article > ' +
      Router.current().params._id;
  },
  allowedRoles: ['manager', 'commercial', 'admin'],
});

Router.route('/manage/import', {
  controller: 'BackOfficeController',
  name: 'importArticles',
  template: 'CSVImport',
  subscriptions: function() {
    Meteor.subscribe('articleTechnicals');
    Meteor.subscribe('csvImportArticles');
  },
  data: {
    articleDefaultsBlueprint,
    technicals: () => ArticleTechnicals.find(),
    lines: () => CSVImportArticles.find(),
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ manager > import';
  },
  allowedRoles: ['manager', 'admin'],
});

Router.route('/manage/categories', {
  controller: 'BackOfficeController',
  name: 'categories',
  template: 'CategoriesEditor',
  subscriptions: function() {
    Meteor.subscribe('categories');
    Meteor.subscribe('articles');
    Meteor.subscribe('articleDefaults');
    Meteor.subscribe('articleTechnicals');
  },
  data: {
    technicals: function() {
      return ArticleTechnicals.find();
    },
    categories: function() {
      return Categories.find();
    },
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ manager > categories';
  },
  allowedRoles: ['manager', 'admin'],
});

Router.route('/manage/salesMargins', {
  controller: 'BackOfficeController',
  name: 'salesMargins',
  template: 'SalesMarginsEditor',
  subscriptions: function() {
    Meteor.subscribe('salesMargins');
    Meteor.subscribe('categories');
    Meteor.subscribe('brands');
    Meteor.subscribe('generalConfiguration');
  },
  data: {
    salesMargins: () => SalesMargins.find({}, {sort: {categoryName: 1}}),
    categories: () => Categories.find({}, {sort: {name: 1}}),
    brands: () => Brands.find(),
    information: () => GeneralConfiguration.findOne(),
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ manager > sales margins';
  },
  allowedRoles: ['commercial', 'admin'],
});

Router.route('/manage/brands', {
  controller: 'BackOfficeController',
  name: 'brands',
  template: 'BrandsEditor',
  subscriptions: function() {
    Meteor.subscribe('brands');
    Meteor.subscribe('articles');
  },
  data: {
    brands: () => Brands.find(),
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ manager > brands';
  },
  allowedRoles: ['manager', 'admin'],
});

Router.route('/manage/images', {
  controller: 'BackOfficeController',
  name: 'images',
  template: 'UploadImageForm',
  subscriptions: function() {
    Meteor.subscribe('images');
  },
  data: {},
  onAfterAction: function() {
    document.title = 'infoRshop ~ manager > images';
  },
  allowedRoles: ['manager', 'admin'],
});

Router.route('/manage/generalConfiguration', {
  controller: 'BackOfficeController',
  name: 'generalConfiguration',
  template: 'GeneralConfiguration',
  subscriptions: function() {
    Meteor.subscribe('generalConfiguration');
  },
  data: {
    information: () => GeneralConfiguration.findOne(),
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ manager > general configuration';
  },
  allowedRoles: ['admin'],
});

Router.route('/manage/technicals', {
  controller: 'BackOfficeController',
  name: 'technicals',
  template: 'TechnicalsEditor',
  subscriptions: function() {
    Meteor.subscribe('articleTechnicals');
  },
  data: {
    technicals: () => ArticleTechnicals.find(),
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ manager > technicals';
  },
  allowedRoles: ['manager', 'admin'],
});

Router.route('/manage/users', {
  controller: 'BackOfficeController',
  name: 'users',
  template: 'UsersEditor',
  subscriptions: function() {
    Meteor.subscribe('employees');
  },
  data: {
    users: () => Meteor.users.find(),
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ manager > users';
  },
  allowedRoles: ['admin'],
});

Router.route('/manage/inventories', {
  controller: 'BackOfficeController',
  name: 'inventories',
  template: 'Inventories',
  waitOn: function() {
    Meteor.subscribe('inventories');
    Meteor.subscribe('articles');
  },
  data: {
    inventory: function() {
      return Inventories.findOne();
    },
    articles: function() {
      return Articles.find();
    },
  },
  onAfterAction: function(){
    document.title = 'infoRshop ~ manager > inventories';
  },
  allowedRoles: ['manager', 'admin'],
});

Router.route('/manage/resupplies', {
  controller: 'BackOfficeController',
  name: 'resupplies',
  template: 'Resupplies',
  waitOn: function() {
    Meteor.subscribe('articlesResupplies');
    Meteor.subscribe('resupplies');
  },
  data: {
    resupply: function() {
      return Resupplies.findOne();
    },
    articlesResupply: function() {
      return Articles.find();
    },
  },
  onAfterAction: function(){
    document.title = 'infoRshop ~ manager > resupplies';
  },
  allowedRoles: ['manager', 'admin'],
});

Router.route('/manage/orders', {
  controller: 'BackOfficeController',
  name: 'orders',
  template: 'Orders',
  waitOn: function() {
    Meteor.subscribe('orders');
  },
  data: {
    orders: () => Orders.find(),
    sentOrders: () => Orders.find({sentAt: {$ne: null}}),
    preparedOrders: () => Orders.find({preparedAt: {$ne: null}, sentAt: {$eq: null}}),
    paidOrders: () => Orders.find({paidAt: {$ne: null}, preparedAt: {$eq: null}}),
  },
  onAfterAction: function(){
    document.title = 'infoRshop ~ manager > orders';
  },
  allowedRoles: ['manager', 'admin'],
});
