
Template.OrderState.events({
  'click [data-action="prepare"]'(event, template) {
    Meteor.call('order.prepare', this._id);
  },
  'click [data-action="send"]'(event, template) {
    Meteor.call('order.send', this._id);
  },
});
