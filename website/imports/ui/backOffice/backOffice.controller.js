
BackOfficeController = RouteController.extend({
  layoutTemplate: 'BackOfficeLayout',
  yieldRegions: {
    'BackOfficeUserMenu': {to: 'user'},
    'BackOfficeHeader': {to: 'header'},
    'BackOfficeNav': {to: 'nav'},
    'BackOfficeFooter': {to: 'footer'},
  },
  subscriptions: function() {
    Meteor.subscribe('generalConfigurationAddress');
  },
  data: {
    information: () => GeneralConfiguration.findOne(),
  },
  onAfterAction: function() {
    clearTimeout(document.redirectHomeAtTimeout);
  },
  authTemplate: 'LoadingBackOffice',
  authRoute: 'Home',
  protected: true,
  allowedGroup: 'infoRshop',
});
