
Template.UsersListItem.helpers({
  email: function() {
    return this.emails[0].address;
  },
  isAdmin: function() {
    return Roles.userIsInRole(this._id, ['admin'], 'infoRshop');
  },
  isManager: function() {
    return Roles.userIsInRole(this._id, ['manager'], 'infoRshop');
  },
  isCommercial: function() {
    return Roles.userIsInRole(this._id, ['commercial'], 'infoRshop');
  },
});
