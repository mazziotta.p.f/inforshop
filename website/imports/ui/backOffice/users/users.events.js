
Template.UsersEditor.events({
  'submit [data-action="create-employee"]'(event, template) {
    event.preventDefault();

    const target = event.target;

    Meteor.call('users.createEmployee', target.email.value, target.password.value);
    target.email.value = '';
    target.password.value = '';
  },
});

Template.UsersListItem.events({
  'click [data-action="remove-employee"]'(event, template) {
    if (!confirm('Remove ?')) return;
    Meteor.call('users.removeEmployee', this._id);
  },
  'click [data-action="toggle-admin-role"]'(event, template) {
    let confirmMessage;
    if (event.target.checked)
      confirmMessage = 'Give ADMIN rights to ' + this.emails[0].address + ' ?';
    else
      confirmMessage = 'Remove ADMIN rights from ' + this.emails[0].address + ' ?';
    if (!confirm(confirmMessage)) return;
    Meteor.call('users.toggleAdminRole', this._id, event.target.checked,
      (error) => {
        if (error) {
          event.target.checked = !event.target.checked;
          sAlert.error(error.details);
        };
      }
    );
  },
  'click [data-action="toggle-manager-role"]'(event, template) {
    Meteor.call('users.toggleManagerRole', this._id, event.target.checked);
  },
  'click [data-action="toggle-commercial-role"]'(event, template) {
    Meteor.call('users.toggleCommercialRole', this._id, event.target.checked);
  },
});
