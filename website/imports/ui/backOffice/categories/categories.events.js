
Template.ArticleDefaultsEditor.events({
  'click [data-view-action="toggle-article-defaults"]'(event, template) {
    let hiddenElement = template.find('.hidden-by-default');
    hiddenElement.style.display = hiddenElement.style.display === 'block' ? 'none' : 'block';
  },
});

Template.ArticleDefaultsTechnicalLineEditor.events({
  'blur [data-action="update-technical-property"]'(event, template) {
    Meteor.call('articleDefaults.updateTechnicalProperty',
      Template.parentData()._id,
      this.name,
      event.target.value
    );
  },
  'click [data-action="remove-technical-property"]'(event, template) {
    if (!confirm('Remove ?')) return;
    Meteor.call('articleDefaults.removeTechnicalProperty',
      Template.parentData()._id,
      this.name,
    );
  },
});

Template.CategoryBindableValuesEditor.events({
  'submit [data-action="add-bindable-value"]'(event, template) {
    event.preventDefault();

    Meteor.call('categories.addBindableValue',
      this._id,
      event.target['bindable-value'].value
    );
    event.target['bindable-value'].value = '';
  },
});

Template.CategoryBindableValueLine.events({
  'click [data-action="remove-bindable-value"]'(event, template) {
    if (!confirm('Remove ?')) return;
    Meteor.call('categories.removeBindableValue',
      Template.parentData()._id,
      this.valueOf()
    );
  },
});

Template.CategoryForm.events({
  'submit [data-action="create-category"]'(event, template) {
    event.preventDefault();
    Meteor.call('categories.create', event.target.name.value);
    event.target.name.value = '';
  },
});

Template.CategoryRow.events({
  'click [data-action="remove-category"]'(event, template) {
    if (!confirm('Remove ?')) return;
    Meteor.call('categories.remove', this._id);
  },
});

Template.ArticleDefaultsEditor.events({
  'submit [data-action="add-technical-property"]'(event, template) {
    event.preventDefault();
    let target = event.target;

    Meteor.call('articleDefaults.addTechnicalProperty', this._id,
      target['technical-property'].value, target['technical-value'].value);

   target['technical-value'].value = '';
  }
});
