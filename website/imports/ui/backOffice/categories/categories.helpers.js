
import { Articles } from '/imports/api/articles/articles.js';
import { ArticleDefaults } from '/imports/api/articles/articleDefaults.js';
import { ArticleTechnicals } from '/imports/api/articles/articleTechnicals.js';

Template.CategoryRow.helpers({
  articlesCount() {
    return Articles.find({categoryId: this._id}).count();
  },
});

Template.ArticleDefaultsEditor.helpers({
  articleDefaults() {
    return ArticleDefaults.findOne({categoryId: this._id});
  },
  technicals() {
    return getAvailableTechnicals(this._id);
  },
  technicalsAreAvailable() {
    let technicals = getAvailableTechnicals(this._id);
    if (technicals && technicals.count()) return true;
  }
});

function getAvailableTechnicals(categoryId) {
  let articleDefaults = ArticleDefaults.findOne({categoryId});
  if (articleDefaults) {
    let technicalsToOmit = ArticleDefaults.findOne({categoryId})
      .technicals.map(function(element) {
        return element.name;
      });
    return ArticleTechnicals.find({name: {$nin: technicalsToOmit}});
  }
}
