
import { Images } from '/imports/api/images/images.js';

Template.UploadImageForm.events({
  'change [data-action="upload-images"]'(event, template) {

    const files = event.target.files;

    if (files) {
      let i = 0
      for (i = 0; i < files.length; i++) {
        const upload = Images.insert({
          file: files[i],
          streams: 'dynamic',
          chunkSize: 'dynamic'
        }, false);

        upload.on('start', function () {
          template.currentUpload.set(this);
        });

        upload.on('end', function (error, file) {
          if (error) {
            sAlert.error('Error during upload: ' + error.reason);
            return;
          }
          template.currentUpload.set(false);
        });

        upload.start();
      }
      sAlert.success('Files uploaded successfully : ' + i);
    }
  }
});
