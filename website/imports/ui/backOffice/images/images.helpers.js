
Template.UploadImageForm.onCreated(function () {
  this.currentUpload = new ReactiveVar(false);
});

Template.UploadImageForm.helpers({
  currentUpload() {
    return Template.instance().currentUpload.get();
  }
});
