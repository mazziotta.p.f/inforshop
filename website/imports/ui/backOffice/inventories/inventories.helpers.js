
import { Template } from 'meteor/templating';

import { Inventories } from '../../../api/inventories/inventories.js';
import { Articles } from '../../../api/articles/articles.js';

Template.ArticlesInventory.helpers({
  inventoryStartedDate(){
    const inventory = this.inventory();

    if (inventory) {
      let date = moment(inventory.createdAt).format('dddd DD MMM YYYY');
      let hour = moment(inventory.createdAt).format('HH:mm');
      var message = "Inventory started " + date + " at " + hour;
    }
    else {
      var message = "Please create a new inventory";
    }
    return message;
  },
});

Template.InventoryArticleRow.helpers({
  name(){
    if (this.articleId) {
      let article = Articles.findOne({_id: this.articleId}, {_id: 0, name: 1});
      if(article) return article.name;
    }
  },
});
