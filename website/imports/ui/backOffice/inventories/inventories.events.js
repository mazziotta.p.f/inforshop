
import { Inventories } from '../../../api/inventories/inventories.js'

Template.Inventories.events({
  'click [data-action="new-inventory"]' (event, template){
    Meteor.call('inventories.create');
  }
});

Template.ArticlesDisplayInventory.events({
  'click' (event, template){
    Meteor.call('inventories.addArticle', this._id, (error) => {
      if (error) sAlert.error(error.reason);
    });
  },
});

Template.InventoryArticleRow.events({
  'blur [data-action="update-quantity"]' (event, template){
    let observedQuantity = parseInt(event.target.value);

    if (isNaN(observedQuantity)) {
      sAlert.error('Please enter a number');
      event.target.value = this.expectedQuantity;
      event.target.focus();
    }
    else if (observedQuantity != this.expectedQuantity) {
      Meteor.call('inventories.updateArticle', this.articleId, observedQuantity);
    }
  },
  'click [data-action="remove-article"]' (event, template){
    Meteor.call('inventories.removeArticle', this.articleId);
  },
});

Template.ArticlesInventory.events({
  'click [data-action="finish-inventory"]' (event, template){
    Meteor.call('inventories.apply');
  },
});
