
Template.GeneralConfiguration.events({
  'click [data-action="reset-configuration"]' (event, Template){
    event.preventDefault();

    Meteor.call('generalConfiguration.reset');
  },
});

Template.VATConfiguration.events({
  'blur [data-action="configure-VAT"]' (event, Template){
    event.preventDefault();

    let vat = parseFloat(event.target.value.replace(',', '.'));
    let currentVat = Router.current().data().information().vat;

    if (vat !== currentVat) {
      if (!isNaN(vat)) {
        Meteor.call('generalConfiguration.VAT.update', vat, (error) => {
          if (error) sAlert.error(error);
          else {
            sAlert.success('VAT updated');
            event.target.value = vat + '%';
          }
        });
      }
      else {
        sAlert.error('Please specify a number for the VAT !');
        event.target.value = currentVat + '%';
        event.target.focus();
      }
    }
  },
});

Template.DefaultMarginConfiguration.events({
  'blur [data-action="configure-default-margin"]' (event, Template){
    event.preventDefault();

    let margin = parseFloat(event.target.value.replace(',', '.'));
    let currentMargin = Router.current().data().information().defaultMargin;

    if (margin !== currentMargin) {
      if (!isNaN(margin)) {
        Meteor.call('generalConfiguration.defaultMargin.update', margin, (error) => {
          if (error) sAlert.error(error);
          else {
            sAlert.success('Default margin updated !');
            event.target.value = margin + '%';
          }
        });
      }
      else {
        sAlert.error('Please specify a number for the default margin !');
        event.target.value = currentMargin + '%';
        event.target.focus();
      }
    }
  },
});

Template.AddressConfiguration.events({
  'blur [data-action="configure-default-sreet"]' (event, Template){
    event.preventDefault();

    let street = event.target.value;
    let currentStreet = Router.current().data().information().street;

    if (street !== currentStreet) {
      if (street !== '') {
        Meteor.call('generalConfiguration.defaultStreet.update', street, (error) => {
          if (error) sAlert.error(error);
          else {
            sAlert.success('Default street updated !');
            event.target.value = street;
          }
        });
      }
      else {
        sAlert.error('Please specify a street !');
        event.target.value = currentStreet;
        event.target.focus();
      }
    }
  },
  'blur [data-action="configure-default-post-code"]' (event, Template){
    event.preventDefault();

    let postCode = event.target.value;
    let currentPostCode = Router.current().data().information().postCode;

    if (postCode !== currentPostCode) {
      if (postCode !== '') {
        Meteor.call('generalConfiguration.defaultPostCode.update', postCode, (error) => {
          if (error) sAlert.error(error);
          else {
            sAlert.success('Default post code updated !');
            event.target.value = postCode;
          }
        });
      }
      else {
        sAlert.error('Please specify a post code !');
        event.target.value = currentPostCode;
        event.target.focus();
      }
    }
  },
  'blur [data-action="configure-default-city"]' (event, Template){
    event.preventDefault();

    let city = event.target.value;
    let currentCity = Router.current().data().information().city;

    if (city !== currentCity) {
      if (city !== '') {
        Meteor.call('generalConfiguration.defaultCity.update', city, (error) => {
          if (error) sAlert.error(error);
          else {
            sAlert.success('Default city updated !');
            event.target.value = city;
          }
        });
      }
      else {
        sAlert.error('Please specify a city !');
        event.target.value = currentCity;
        event.target.focus();
      }
    }
  },
});

Template.DeliveryFeeLine.events({
  'blur [data-action="update-delivery-fee"]'(event, template) {
    if (!confirm('Remove ?')) return;
    let fee = parseFloat(event.target.value.replace(',', '.'));
    let minimumDistance = template.data.minimumDistance;

    if (!isNaN(fee) && fee !== template.data.fee) {
      Meteor.call('generalConfiguration.updateDeliveryFee',
        parseInt(minimumDistance), fee, (error) => {
          if (error) sAlert.error(error);
          else sAlert.success(minimumDistance + 'km fee updated', 'success');
        });
    }
  },
  'click [data-action="remove-delivery-fee"]'(event, template) {
    if (!confirm('Remove ?')) return;
    let minimumDistance = template.data.minimumDistance;
    Meteor.call('generalConfiguration.removeDeliveryFee', minimumDistance,
      (error) => {
        if (error) sAlert.error(error);
        else sAlert.success('Removed '+ minimumDistance + 'km fee successfully');
      });
  },
});

Template.DeliveryFeesConfiguration.events({
  'submit [data-action="add-delivery-fee"]'(event, template) {

    event.preventDefault();

    let minimumDistance = parseFloat(event.target.distance.value.replace(',', '.'));
    let fee = parseFloat(event.target.fee.value.replace(',', '.'));

    Meteor.call('generalConfiguration.addDeliveryFee', minimumDistance, fee,
      (error) => {
        if (error) sAlert.error(error);
        else sAlert.success('Added '+ minimumDistance + 'km fee successfully');
      });
  },
});
