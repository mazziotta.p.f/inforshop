
Template.DeliveryFeesConfiguration.helpers({
  sortedDeliveryFees() {
    let config = Router.current().data().information();
    if (config && config.deliveryFees) {
      return config.deliveryFees.sort(
        (a, b) => a.minimumDistance > b.minimumDistance
      );
    }
  },
});
