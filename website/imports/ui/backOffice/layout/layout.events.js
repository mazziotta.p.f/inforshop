
Template.BackOfficeUserMenu.events({
  'click [data-view-action="display-account-form"]'(event, template) {
    template.find('.account-form').style.display = 'block';
  },
  'click [data-view-action="close-account-form"]'(event, template) {
    template.find('.account-form').style.display = 'none';
  },
});
