import { GeneralConfiguration } from '/imports/api/generalConfiguration/generalConfiguration.js';

Template.BackOfficeUserMenu.helpers({
  userDenomination: function() {
    if (Meteor.user()){
      let profile = Meteor.user().profile;
      if (profile && profile.name) return profile.name;
      else return Meteor.user().emails[0].address;
    }
  },
});

Template.BackOfficeNav.helpers({
  navEntries: function() {
    return [{
      navEntry : 'Home',
      routeName : 'backOfficeHome',
    },{
      navEntry : 'Brands',
      routeName : 'brands',
    },{
      navEntry : 'Categories',
      routeName : 'categories',
    },{
      navEntry : 'Technicals',
      routeName : 'technicals',
    },{
      navEntry : 'Margins',
      routeName : 'salesMargins',
    },{
      navEntry : 'Import',
      routeName : 'importArticles',
    },{
      navEntry : 'Images',
      routeName : 'images',
    },{
      navEntry : 'Articles',
      routeName : 'articlesEditor',
    },{
      navEntry: 'Configuration',
      routeName: 'generalConfiguration',
    },{
      navEntry : 'Inventories',
      routeName : 'inventories',
    },{
      navEntry : 'Resupplies',
      routeName : 'resupplies',
    },{
      navEntry: 'Users',
      routeName: 'users',
    },{
      navEntry: 'Orders',
      routeName: 'orders',
    }];
  },
});

Template.BackOfficeNavEntry.helpers({
  isCurrentRoute: function() {
    let navRouteName = Router.current().route.getName();
    if (navRouteName.startsWith('article')) {
      return this.routeName.startsWith('article');
    }
    return Router.current().route.getName() === this.routeName;
  },
  isAuthorized: function() {
    let route = Router.routes[this.routeName];
    if (route)
      return Roles.userIsInRole(Meteor.userId(), route.options.allowedRoles, 'infoRshop');
    return false;
  },
});

Template.BackOfficeFooter.helpers({
  information(){
    // general information to display in the footer
    return GeneralConfiguration.findOne();
  },
});
