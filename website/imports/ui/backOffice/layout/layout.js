import './templates/components/user.html';
import './templates/components/header.html';
import './templates/components/nav.html';
import './templates/components/footer.html';

import './templates/layout.html';

import './style/default.css';
import './style/mobile.css';

import './layout.helpers.js';
import './layout.events.js';
