Template.TechnicalsEditor.events({
  'submit [data-action="create-technical"]'(event, template) {
    event.preventDefault();
    Meteor.call('articleTechnicals.create', event.target.name.value);
    event.target.name.value = '';
  },
});

Template.TechnicalsListItem.events({
  'click [data-action="remove-technical"]'(event, template) {
    if (!confirm('Remove ?')) return;
    Meteor.call('articleTechnicals.remove', this._id);
  },
});
