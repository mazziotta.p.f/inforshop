
import { Template } from 'meteor/templating';

import { CSVImportArticles } from '/imports/api/csvImport/csvImport.js';


Template.ImportTable.onCreated(
  function() {
    Meteor.subscribe('csvImportArticles');
  }
);

Template.CSVImport.helpers({
  displayTable() {
    return CSVImportArticles.find().count() > 0;
  },
});

Template.ImportMapper.helpers({
  keys() {
    let article = Router.current().data().articleDefaultsBlueprint;

    return Object.keys(article).filter(function(key) {
      return key !== 'technicals';
    });
  },
  technicalKeys() {
    let technicals = Router.current().data().technicals();
    if (technicals.count()) {
      return technicals.map(function(element) {
        return 'technicals.' + element.name;
      });
    }
  },
});

Template.ImportHeaders.helpers({
  keys() {
    const line = CSVImportArticles.findOne();

    if (line) {
      return Object.keys(line).filter(function(element) {
        return element.startsWith('_') === false;
      });
    }
  },
});

Template.ImportRow.helpers({
  values() {
    if(this){
      return Object.values(this);
    }
  },
});


Template.Upload.onCreated( () => {
  Template.instance().uploading = new ReactiveVar (false);
});

Template.Upload.helpers({
  uploading() {
    return Template.instance().uploading.get();
  }
});
