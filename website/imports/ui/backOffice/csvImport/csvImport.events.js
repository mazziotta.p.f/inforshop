Template.CSVImport.events({
  'click button[data-action="remove"]' (event, template) {
    if (!confirm('Remove ?')) return;
    // from input -> td -> tr -> td[]
    const rowCells = event.target.parentElement.parentElement.children;
    // from td[] -> last td -> input -> value
    const csvArticleId = rowCells[rowCells.length -1].children[0].value;

    Meteor.call('csvImportArticles.remove', csvArticleId);
  },
  'submit [data-action="beginImport"]' (event, template) {
    event.preventDefault();

    let schema = {};
    // get header values as keys
    let keys = template.findAll('th input').map(function(input) {
      if (input) {
        return input.value;
      }
    });
    // get header input values as values
    let values = template.findAll('select').map(function(select) {
      let option = select.selectedOptions[0];
      if (option) {
        return option.value;
      }
    });

    // merge keys and values into object schema
    for (var i = 0; i < keys.length; i++) {
      schema[keys[i]] = values[i];
    }

    if (Object.keys(schema).length) {
      Meteor.call('csvImportArticles.importFromCSVArticles', schema);
    }
  },
});

Template.Upload.events({
  'change [name="uploadCSV"]' (event, template) {
    template.uploading.set(true);
    Papa.parse(event.target.files[0], {
      header: true,
      skipEmptyLines: true,
      beforeFirstChunk: function( chunk ) {
        let rows = chunk.split( /\r\n|\r|\n/ );
        let headings = rows[0].split( ',' );
        let cleanHeadings = [];
        headings.forEach(function(heading){
          cleanHeadings.push(heading.replace(/[^a-zA-Z0-9]/g, ''));
        });
        rows[0] = cleanHeadings.join();
        return rows.join( '\n' );
      },
      //TODO Use chunk instead of complete
      complete: function (results, files) {
        Meteor.call('csvImportArticles.parseUpload', results.data, (error, response) => {
          if (error) sAlert.error(error);
          else sAlert.success('Upload done!');
          template.uploading.set(false);
        });
      },
    });
  },
});

Template.ImportTable.events({
  'click [data-view-action="auto-bind-columns"]'(event, template) {
    event.preventDefault();

    var selects = template.findAll('select');
    for (var i = 0; i < selects.length;i++) {
    	if(i+1 < selects.length) selects[i].selectedIndex = i + 1;
    }
  },

  'blur tr[data-action="editCSVArticle"] td input[type="text"]'(event, template) {
    const target = event.target;
    // from input -> td
    const cell = target.parentElement;
    // from input -> td -> tr -> td[]
    const rowCells = target.parentElement.parentElement.children;
    const cellIndex = Array.from(rowCells).indexOf(cell);
    // find out header name
    const header = template.findAll('th')[cellIndex].children[0].value;
    // from td[] -> last td -> input -> value
    const csvArticleId = rowCells[rowCells.length -1].children[0].value;

    let updateObject = {};
    updateObject[header] = target.value;

    Meteor.call('csvImportArticles.update', csvArticleId, updateObject);
  },
});
