
import { Template } from 'meteor/templating';

import { Categories } from '/imports/api/categories/categories.js';
import { SalesMargins } from '/imports/api/salesMargins/salesMargins.js';
import { Brands } from '/imports/api/brands/brands.js';
import { GeneralConfiguration } from '/imports/api/generalConfiguration/generalConfiguration.js';

Template.SalesMarginsForm.onCreated(function(){
  this.state = new ReactiveDict();
  this.state.set('dropDownList', Categories.find().fetch());
});

Template.SalesMarginsForm.helpers({
  availableCategories(){
    return getAvailablecategories();
  },
  information(){
    return GeneralConfiguration.findOne();
  },
});

Template.SalesMarginsEditor.helpers({
  availableCategories(){
    return getAvailablecategories().count();
  },
});

Template.SalesMarginsRow.helpers({
  categoryName(){
    const category = Categories.findOne(
      {_id: this.categoryId},
      {_id: 0, name: 1});

    if (category) {
      return category.name;
    }
  },
});

Template.BrandsDropDownList.helpers({
  availableBrands(){
    return getAvailableBrands(this.categoryId);
  },
  information(){
    return GeneralConfiguration.findOne();
  },
});

Template.BrandsMarginEditorToggle.helpers({
  availableBrands(){
    return getAvailableBrands(this.categoryId).count();
  },
  brands(){
    return this.brandMargins;
  },
});

Template.BrandMarginEditor.helpers({
  brandName(){
    return Brands.findOne(
      {_id: this.brandId},
      {_id: 0, name: 1});
  },
});

// Functions used in helpers

// Return all categories that have no default sales margin
function getAvailablecategories() {
  let categories = SalesMargins.find({}, {_id: 0, categoryId: 1})
    .map(function(salesMargins){
      return salesMargins.categoryId;
    });

  return Categories.find({_id: {$nin: categories}}, {_id: 1, name: 1});
}

// Return all brands that have no sales margin in the selected category
function getAvailableBrands(categoryId) {
  let brands = SalesMargins.findOne({categoryId: categoryId}, { fields: {brandMargins: 1}});

  var brandsArr = [];
  if (brands) {
    for (var i = 0; i < brands.brandMargins.length; i++) {
      brandsArr.push(brands.brandMargins[i].brandId);
    }
  }

  var brandsInDropDownList = Brands.find({_id: {$nin: brandsArr}}, {_id: 1, name: 1});
  return brandsInDropDownList;
}
