
Template.SalesMarginsForm.events({
  'submit [data-action="create-sales-margin"]' (event, template) {
    event.preventDefault();

    let e = event.target['category-margin'];
    let categoryId = e.value;
    let categoryName = e.options[e.selectedIndex].text;
    let salesMargin = parseFloat(event.target['sales-margin'].value.replace(',', '.'));

    Meteor.call('salesMarginsCategory.create', categoryId, categoryName, salesMargin);
  }
});

Template.SalesMarginsRow.events({
  'blur [data-action="edit-category-default-sales-margin"]' (event, template) {
    event.preventDefault();

    let categorySalesMargin = parseFloat(event.target.value.replace(',', '.'));
    let salesMarginId = this._id;

    // check if value is different before invoquing Meteor.call
    if (categorySalesMargin !== this.defaultMargin) {
      if (!isNaN(categorySalesMargin)) {
        Meteor.call('salesMarginsCategory.update', salesMarginId, categorySalesMargin, function (error) {
          if (error && error === 'No sales margins ID') {
            sAlert.error('No sales margins ID !');
          }
          else {
            sAlert.success('Sales margin updated');
            event.target.value = categorySalesMargin;
          }
        });
      }
      else {
        sAlert.error('Please specify a number for the sales margin !');
        event.target.value = this.defaultMargin;
        event.target.focus();
      }
    }
  },
  'click [data-action="remove-category-margin"]' (event, template) {
    event.preventDefault();
    if (!confirm('Remove ?')) return;

    let salesMarginId = this._id;

    Meteor.call('salesMarginsCategory.remove', salesMarginId, function (error) {
      if (error && error === 'No sales margin ID') {
        sAlert.error('No sales Margin ID !');
      }
    });
  }
});

Template.BrandsMarginEditorToggle.events({
  'click [data-view-action="toggle-brands-editor"]'(event, template) {
    let hiddenElement = template.find('.hidden-by-default');
    hiddenElement.style.display = hiddenElement.style.display === 'block' ? 'none' : 'block';
  },
});

Template.BrandsDropDownList.events({
  'submit [data-action="create-brand-sales-margin"]'(event, template) {
    event.preventDefault();

    let brandDropDownList = event.target.firstElementChild.firstElementChild;

    let salesMarginId    = this._id;
    let brandId           = brandDropDownList.value;
    let brandName         = brandDropDownList.options[brandDropDownList.selectedIndex].text;
    let brandSalesMargin  = parseFloat(event.target.firstElementChild.children[1].value.replace(',', '.'));

    Meteor.call('salesMarginsBrand.create', salesMarginId, brandId, brandName, brandSalesMargin);
  },
});

Template.BrandMarginEditor.events({
  'click [data-action="remove-brand-sales-margin"]'(event, template){
    if (!confirm('Remove ?')) return;
    let salesMarginId = Template.parentData()._id;
    let brandId       = this.brandId;

    Meteor.call('salesMarginsBrand.remove', salesMarginId, brandId);
  },
  'blur [data-action="update-brand-sales-margin"]'(event, template){
    let salesMarginId    = Template.parentData()._id;
    let brandId          = this.brandId;
    let brandSalesMargin = parseFloat(event.target.value.replace(',', '.'));

    // check if value is different before invoquing Meteor.call
    if (brandSalesMargin !== this.margin) {
      if (!isNaN(brandSalesMargin)) {
        Meteor.call('salesMarginsBrand.update', salesMarginId, brandId, brandSalesMargin, function (error) {
          if (error && error === 'No sales margins ID') {
            sAlert.error('No sales margins ID !');
          }
          else {
            sAlert.success('Sales margin updated');
            event.target.value = brandSalesMargin;
          }
        });
      }
      else {
        sAlert.error('Please specify a number for the sales margin !');
        event.target.value = this.margin;
        event.target.focus();
      }
    }
  }
});
