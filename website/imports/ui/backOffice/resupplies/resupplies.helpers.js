
import { Template } from 'meteor/templating';

import { Resupplies } from '../../../api/resupplies/resupplies.js';
import { Articles } from '../../../api/articles/articles.js';

Template.ArticlesResupply.helpers({
  resupplyStartedDate(){
    const resupply = this.resupply();

    if (resupply) {
      let date = moment(resupply.createdAt).format('dddd DD MMM YYYY');
      let hour = moment(resupply.createdAt).format('HH:mm');
      var message = "Resupply started " + date + " at " + hour;
    }
    else {
      var message = "Start a new resupply";
    }
    return message;
  },
});

Template.InventoryResupplyArticleRow.helpers({
  name(){
    if (this.articleId) {
      let article = Articles.findOne({_id: this.articleId}, {_id: 0, name: 1});
      if(article) return article.name;
    }
  },
});
