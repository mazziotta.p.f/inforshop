import { Resupplies } from '../../../api/resupplies/resupplies.js'

Template.Resupplies.events({
  'click [data-action="new-resupply"]' (event, Template){
    Meteor.call('resupplies.create');
  }
});

Template.ArticlesDisplayResupply.events({
  'click' (event, Template){
    Meteor.call('resupplies.addArticle', this._id, (error) => {
      if (error) sAlert.error(error.reason);
    });
  },
});

Template.InventoryResupplyArticleRow.events({
  'blur [data-action="update-resupply-quantity"]' (event, Template){
    let quantity = parseInt(event.target.value);

    if (isNaN(quantity)) {
      sAlert.error('Please enter a number of article to resupply !');
      event.target.value = this.quantity;
      event.target.focus();
    }
    else if (quantity != this.quantity) {
      Meteor.call('resupplies.updateArticle', this.articleId, quantity);
    }

  },
  'click [data-action="remove-article"]' (event, template){
    Meteor.call('resupplies.removeArticle', this.articleId);
  },
});

Template.ArticlesResupply.events({
  'click [data-action="finish-resupply"]' (event, template){
    Meteor.call('resupplies.apply');
  },
});
