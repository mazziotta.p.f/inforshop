
Template.BrandForm.events({
  'submit [data-action="create-brand"]'(event, template) {
    event.preventDefault();
    Meteor.call('brands.create', event.target.name.value);
    event.target.name.value = '';
  },
});

Template.BrandRow.events({
  'click [data-action="remove-brand"]'(event, template) {
    if (!confirm('Remove ?')) return;
    Meteor.call('brands.remove', this._id);
  },
});

Template.BrandBindableValuesEditor.events({
  'submit [data-action="add-bindable-value"]'(event, template) {
    event.preventDefault();

    Meteor.call('brands.addBindableValue',
      this._id,
      event.target['bindable-value'].value
    );
    event.target['bindable-value'].value = '';
  },
});

Template.BrandBindableValueLine.events({
  'click [data-action="remove-bindable-value"]'(event, template) {
    if (!confirm('Remove ?')) return;
    Meteor.call('brands.removeBindableValue',
      Template.parentData()._id,
      this.valueOf()
    );
  },
});
