
import { Articles } from '/imports/api/articles/articles.js';

Template.BrandRow.helpers({
  articlesCount() {
    return Articles.find({brandId: this._id}).count();
  },
});
