
Template.ArticlesSearch.events({
  'keyup [data-view-action="toggle-search-result"]'(event, template) {
    let searchResultElement = template.find('.search-result');
    let searchFilter = event.target.value;

    if (searchFilter) {
      searchResultElement.style.display = 'block';
    } else {
      searchResultElement.style.display = 'none';
    }
  },
  'click [data-view-action="close-search-result"]'(event, template) {
    template.find('.search-result').style.display = 'none';
  },
  'focus [data-view-action="toggle-search-result"]'(event, template) {
    if (event.target.value) {
      template.find('.search-result').style.display = 'block';
    }
  },
});
