import './templates/search.html';
import './style/search.css';
import './style/search.mobile.css';

import './searchBarArticles.helpers.js';
import './searchBarArticles.events.js';
