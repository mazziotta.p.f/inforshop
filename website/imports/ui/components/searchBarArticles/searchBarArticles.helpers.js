
import { ArticlesIndex } from '/imports/api/articles/articles.js'

Template.ArticlesSearch.onCreated(() => {
  Meteor.subscribe('lightArticles');
  Meteor.subscribe('images');
});

Template.ArticlesSearch.helpers({
  articlesIndex: function() {
    return ArticlesIndex;
  },
});
