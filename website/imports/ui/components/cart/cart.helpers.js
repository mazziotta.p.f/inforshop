
import { Articles } from '/imports/api/articles/articles.js';
import { Images } from '/imports/api/images/images.js';
import { Promotions } from '/imports/api/promotions/promotions.js';

Template.CartEntry.onCreated(() => {
  Meteor.subscribe('lightArticles');
  Meteor.subscribe('images');
});

Template.Cart.helpers({
  entries: () => Cart.entries,
  totalPrice: function() {
    let total = 0;
    if (Cart.entries)
      Cart.entries.forEach(function(entry) {
        let article = Articles.findOne({_id: entry.articleId});
        if (article) total += entry.quantity * Calculator.calculateDiscountedPrice(article);
      });
    return total.toFixed(2);
  },
});

Template.CartEntry.helpers({
  article: function() {
    return Articles.findOne({_id: this.articleId});
  },
  image: function() {
    let article = Articles.findOne({_id: this.articleId});
    if (article) return Images.findOne({name: article.imageName});
  },
  hasPromotions() {
    let article = Articles.findOne({_id: this.articleId});
    if (article) return Promotions.find({articleId: article._id}).count() > 0;
  },
  price: function() {
    let article = Articles.findOne({_id: this.articleId});
    if (article) return (article.price * this.quantity).toFixed(2);
  },
  totalDiscount() {
    let article = Articles.findOne({_id: this.articleId});
    if (article) {
      let promotions = Promotions.find({articleId: article._id});
      return (Calculator.calculateCumulativeDiscount(promotions) * 100).toFixed(0);
    }
  },
  discountedPrice: function() {
    let article = Articles.findOne({_id: this.articleId});
    if (article) return (Calculator.calculateDiscountedPrice(article) * this.quantity).toFixed(2);
  },
  isAtMaximumQuantity: function() {
    return Cart.isAtMaximumQuantity(this.articleId);
  },
  isAtMinimumQuantity: function() {
    return Cart.isAtMinimumQuantity(this.articleId);
  },
});
