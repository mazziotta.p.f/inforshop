
import './templates/cart.html';
import './style/cart.css';

import './cart.helpers.js';
import './cart.events.js';

import { Session } from 'meteor/session'

import { Carts } from '/imports/api/carts/carts.js';
import { Articles } from '/imports/api/articles/articles.js';

const MAXIMUM_ARTICLE_ORDER_QUANTITY = 50;

class CartManager {

  cart;
  cumulativeQuantity;

  constructor() {
    this.cart = new ReactiveArray();
    this.cumulativeQuantity = new ReactiveVar();
    this.load();
  }

  load(importSessionArticles) {
    this.cumulativeQuantity.set(0);
    if(Security.isConnected()) {
      this.import = importSessionArticles;
      Meteor.subscribe('lightArticles');
      Meteor.subscribe('carts', this.loadConnectedCart);
    }
    else {
      Meteor.subscribe('lightArticles', this.loadSessionData);
    }
  }

  loadConnectedCart = () => {
    if(this.import) {
      this.clear();
      let cartEntries = this.fetchSessionCartEntries();
      if (cartEntries && cartEntries.constructor === Array) {
        cartEntries.forEach((entry) => {
          this.add(entry.articleId, entry.quantity)
        });
        this.clear(true);
      }
    }
    this.cumulativeQuantity.set(this.computeQuantity());
    this.info();
  }

  loadSessionData = () => {
    let cartEntries = this.fetchSessionCartEntries();
    if (!cartEntries || !cartEntries.constructor === Array) return;
    if(!this.entries.length) cartEntries.forEach(this.addObjectEntry);
    this.cumulativeQuantity.set(this.computeQuantity());
    if (cartEntries.length !== this.cart.length) this.save();
    else this.info();
  }

  fetchSessionCartEntries = () => {
    let sessionCart = Session.get('cart');
    if (!sessionCart) return;
    return JSON.parse(sessionCart);
  }

  addObjectEntry = (entry) => {
    if (Articles.findOne({_id: entry.articleId}))
      this.cart.push(entry);
  }

  save() {
    Session.setPersistent('cart', JSON.stringify(this.cart.array()));
    this.info();
  }

  add(articleId, quantity) {
    if(Security.isConnected()) {
      Meteor.call('carts.addArticle', articleId, quantity, () => {
        this.cumulativeQuantity.set(this.computeQuantity());
        this.info();
      });
      return;
    }
    let foundEntry = this.cart.find((entry) => {
      if(entry.articleId === articleId) return entry;
    });
    if (foundEntry) {
      let totalQuantity = foundEntry.quantity + quantity;
      if (totalQuantity <= 0 || totalQuantity > MAXIMUM_ARTICLE_ORDER_QUANTITY) return;
      foundEntry.quantity = totalQuantity;
      this.cart.changed();
    }
    else {
      if (quantity <= 0 || quantity > MAXIMUM_ARTICLE_ORDER_QUANTITY) return;
      let article = Articles.findOne({_id: articleId});
      if(!article) return;
      this.cart.push({
        articleId,
        slug: article.slug,
        quantity
      });
    }
    this.cumulativeQuantity.set(this.computeQuantity());
    this.sort();
    this.save();
  }

  isAtMaximumQuantity(articleId) {
    if (!this.entries) return;
    let foundEntry = this.entries.find((entry) => {
      if(entry.articleId === articleId) return entry;
    });
    if (foundEntry) return foundEntry.quantity === MAXIMUM_ARTICLE_ORDER_QUANTITY;
  }

  isAtMinimumQuantity(articleId) {
    if (!this.entries) return;
    let foundEntry = this.entries.find((entry) => {
      if(entry.articleId === articleId) return entry;
    });
    if (foundEntry) return foundEntry.quantity === 1;
  }

  sort() {
    let sortedEntries = this.cart.list().sort(this.compareEntries);
    this.cart = new ReactiveArray(sortedEntries);
  }

  compareEntries(a, b) {
    if (a.slug < b.slug)
      return -1;
    if (a.slug > b.slug)
      return 1;
    return 0;
  }

  remove(articleId) {
    if(Security.isConnected()) {
      Meteor.call('carts.removeArticle', articleId, () => {
        this.cumulativeQuantity.set(this.computeQuantity());
        this.info();
      });
      return;
    }
    let foundEntry = this.cart.find((entry) => {
      if(entry.articleId === articleId) return entry;
    });
    if (foundEntry) {
      this.cart.remove(foundEntry);
    }
    this.cumulativeQuantity.set(this.computeQuantity());
    this.save();
  }

  setQuantity(articleId, quantity) {
    return this.cart.find((entry) => {
      if(entry.articleId === articleId) {
        entry.quantity = quantity;
        return true;
      }
      return false;
    });
    this.cumulativeQuantity.set(this.computeQuantity());
    this.save();
  }

  clear(forceSessionClear) {
    if(Security.isConnected() && !forceSessionClear) {
      Meteor.call('carts.clear');
      this.cumulativeQuantity.set(0);
      return;
    }
    this.cart.clear();
    this.cumulativeQuantity.set(0);
    Session.clear('cart');
    if(!forceSessionClear) this.info();
  }

  info() {
    if (this.quantity)
      if (this.quantity > 1)
        this.showInfoAlert('Votre panier contient ' + this.quantity + ' articles');
      else
        this.showInfoAlert('Votre panier contient 1 article');
    else
      this.showInfoAlert('Votre panier est vide');
  }

  showInfoAlert(message) {
    if (this.alertTmp) sAlert.close(this.alertTmp);
    this.alertTmp = sAlert.info(message, {stack: false});
  }

  get entries() {
    if (Security.isConnected()) {
      const userCart = Carts.findOne({owner: Meteor.userId()});
      if (userCart) return userCart.entries.sort(this.compareEntries);
    }
    else return this.cart.list();
  }

  get quantity() {
    return this.cumulativeQuantity.get();
  }

  computeQuantity() {
    let total = 0;
    if (this.entries)
      this.entries.forEach((entry) => {total += entry.quantity});
    return total;
  }
}

Cart = new CartManager();
