
Template.Cart.events({
  'click [data-view-action="close-cart-manager"]'(event, template) {
    template.find('.cart-manager').style.display = 'none';
  },
  'click [data-action="clear-cart"]'(event, template) {
    if (!confirm('Vider la panier ?')) return;
    Cart.clear();
  },
  'click [data-action="order-cart"]'(event, template) {
    if (Security.isConnected())
      Meteor.call('orders.create', (error, result) => {
        if (result) Router.go('orderValidation', {orderId: result});
      });
    else
      alert('Afin de commander le contenu de votre panier, veuillez vous connecter.');
  },
});

Template.CartEntry.events({
  'click [data-action="remove-cart-entry"]'(event, template) {
    Cart.remove(this.articleId);
  },
  'click [data-action="increment-quantity"]'(event, template) {
    Cart.add(this.articleId, 1);
  },
  'click [data-action="decrement-quantity"]'(event, template) {
    Cart.add(this.articleId, -1);
  },
});
