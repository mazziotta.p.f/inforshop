AccountsTemplates.configure({
  onSubmitHook: ( error, state ) => {
    if ( !error && state === 'signIn' ) {
      if (Cart.quantity) {
        Cart.load(confirm('Importer les articles de votre panier non connecté ?'));
      } else {
        Cart.load();
      }
    }
  },
  onLogoutHook: ( error, state ) => {
    Cart.load();
  }
});
