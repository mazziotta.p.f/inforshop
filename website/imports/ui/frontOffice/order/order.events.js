
Template.OrderValidation.events({
  'click [data-action="validate-order"]'(event, template) {
    const orderId = this.order()._id;
    Meteor.call('order.validate', orderId, (error, result) => {
      if (!error && result) Router.go('orderPayment', {orderId}, {replaceState: true});
    });
  },
});

Template.OrderPayment.events({
  'click [data-action="invalidate-order"]'(event, template) {
    const orderId = this.order()._id;
    Meteor.call('order.paymentInformationIsUnset', orderId, (error, result) => {
      if (!error && result) Router.go('orderValidation', {orderId}, {replaceState: true});
    });
  },
  'click [data-action="pay-via-api"]'(event, template) {
    const orderId = this.order()._id;
    Meteor.call('order.paymentInformationIsSet', orderId);
  },
  'click [data-action="pay-order"]'(event, template) {
    const orderId = this.order()._id;
    Meteor.call('order.pay', orderId, (error, result) => {
      if (!error && result) {
        Cart.clear();
        Router.go('orderConfirmation', {orderId}, {replaceState: true});
      }
    });
  },
});
