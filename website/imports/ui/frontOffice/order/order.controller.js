
OrderController = FrontOfficeController.extend({
  layoutTemplate: 'OrderLayout',
  yieldRegions: {
    'FrontOfficeUserMenu': {to: 'user'},
    'FrontOfficeHeader': {to: 'header'},
    'FrontOfficeFooter': {to: 'footer'},
    'FrontOfficeNav': {to: 'nav'},
  },
});
