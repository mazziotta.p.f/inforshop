import './style/order.css';
import './style/order.mobile.css';
import './templates/layout.html';
import './templates/order.html';

import './order.controller.js';

import './order.events.js';
import './order.helpers.js';

import './order.routes.js';
