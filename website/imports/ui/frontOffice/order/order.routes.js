
import { Orders } from '/imports/api/orders/orders.js';

Router.route('/commande/valider/:orderId', {
  controller: 'OrderController',
  name: 'orderValidation',
  template: 'OrderValidation',
  subscriptions: function() {
    Meteor.subscribe('lightArticles');
    Meteor.subscribe('images');
    Meteor.subscribe('currentPromotions');
    Meteor.subscribe('currentOrders');
  },
  data: {
    order: () => Orders.findOne({_id: Router.current().params.orderId}),
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ commande > valider';
  },
});

Router.route('/commande/payer/:orderId', {
  controller: 'OrderController',
  name: 'orderPayment',
  template: 'OrderPayment',
  subscriptions: function() {
    Meteor.subscribe('lightArticles');
    Meteor.subscribe('images');
    Meteor.subscribe('currentPromotions');
    Meteor.subscribe('currentOrders');
  },
  data: {
    order: () => Orders.findOne({_id: Router.current().params.orderId}),
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ commande > payer';
  },
});

Router.route('/commande/confirmation/:orderId', {
  controller: 'OrderController',
  name: 'orderConfirmation',
  template: 'OrderConfirmation',
  subscriptions: function() {
    Meteor.subscribe('lightArticles');
    Meteor.subscribe('images');
    Meteor.subscribe('currentPromotions');
    Meteor.subscribe('currentOrders');
  },
  data: {
    order: () => Orders.findOne({_id: Router.current().params.orderId}),
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ commande > confirmation';
  },
});
