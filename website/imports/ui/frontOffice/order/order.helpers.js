
import { Articles } from '/imports/api/articles/articles.js';
import { Images } from '/imports/api/images/images.js';
import { Promotions } from '/imports/api/promotions/promotions.js';
import { GeneralConfiguration } from '/imports/api/generalConfiguration/generalConfiguration.js';

Template.OrderValidation.onCreated(function() {
  Template.OrderValidation.originMarker = new ReactiveVar(null);
});

Template.OrderValidation.onRendered(function() {
  GoogleMaps.load({
    key: 'AIzaSyBiK_DN9azwXVRzo7YO-iGmqVmbrJHwpqc',
    libraries: 'geometry,places'
  });
  GoogleMaps.ready('map', function(map) {
    map = map.instance;
    let searchBoxElement = document.getElementById('search-address');
    let searchBox = new google.maps.places.SearchBox(searchBoxElement);

    Meteor.call('generalConfiguration.getServerAddress', (error, result) => {
      centerMapOnOrigin(map, result);
    });

    var markers = [];
    searchBox.addListener('places_changed', locateAndUpdateOrder);

    function locateAndUpdateOrder() {
      var places = searchBox.getPlaces();

      if (places.length == 0) return;

      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }

        markers.push(new google.maps.Marker({
          map,
          title: place.name,
          position: place.geometry.location
        }));

        // Only geocodes have viewport.
        if (place.geometry.viewport) bounds.union(place.geometry.viewport);
        else bounds.extend(place.geometry.location);
      });
      map.fitBounds(bounds);
      const origin = Template.OrderValidation.originMarker.get().position;
      const destination = markers[0].position;
      const distance = calculateDistance(origin, destination);
      const orderId = Router.current().params.orderId;
      Meteor.call('order.updateAddress', orderId, searchBoxElement.value);
      Meteor.call('order.updateDeliveryFee', orderId, distance);
    }
  });
});

function calculateDistance(a, b) {
  return (google.maps.geometry.spherical.computeDistanceBetween(a, b) / 1000);
}

function centerMapOnOrigin(map, address) {
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode({address}, function(results, status) {
      if (status != google.maps.GeocoderStatus.OK) return;
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
        map,
        position: results[0].geometry.location
      });
      Template.OrderValidation.originMarker.set(marker);
    }
  );
}

Template.OrderValidation.helpers({
  mapOptions: function() {
    if (GoogleMaps.loaded()) {
      return {
        center: new google.maps.LatLng(0, 0),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
    }
  },
});

Template.OrderArticle.helpers({
  imageLink: function(article) {
    if (article) {
      const image = Images.findOne({name: article.imageName});
      if (image) return image.link();
    }
  },
  priceTimesQuantity: (article) => {
    if (article) {
      if (article.promotions.length)
        return Formatter.price(article.quantity * article.discountedPrice);
      else return Formatter.price(article.quantity * article.price);
    }
  },
});
