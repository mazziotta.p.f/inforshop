
import './frontOffice.routes.js';
import './frontOffice.controller.js';

import './layout/layout.js';

import './home/home.js';
import './articles/articles.js';
import './order/order.js';
