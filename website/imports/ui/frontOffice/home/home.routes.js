
import { Articles } from '/imports/api/articles/articles.js';
import { Images } from '/imports/api/images/images.js';
import { Categories } from '/imports/api/categories/categories.js';

Router.route('/', {
  controller: 'HomeController',
  name: 'home',
  template: 'Home',
  subscriptions: function() {
    Meteor.subscribe('lightArticles');
    Meteor.subscribe('images');
    Meteor.subscribe('categories');
    Meteor.subscribe('currentPromotions');
  },
  data: {
    categories: function() {
      return Categories.find({}, {sort: {slug: 1}});
    },
    articles: function() {
      return Articles.find();
    },
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ accueil';
  },
});
