
HomeController = FrontOfficeController.extend({
  layoutTemplate: 'FrontOfficeLayout',
  yieldRegions: {
    'FrontOfficeUserMenu': {to: 'user'},
    'FrontOfficeAside': {to: 'aside'},
    'FrontOfficeHeader': {to: 'header'},
    'FrontOfficeFooter': {to: 'footer'},
    'FrontOfficeNav': {to: 'nav'},
    'FrontOfficePresentation': {to: 'presentation'},
    'ArticlesSearch': {to: 'search'},
  },
});
