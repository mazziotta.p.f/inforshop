
Template.BestPromotions.onCreated(function () {
  this.state = new ReactiveDict();
  Meteor.call('promotions.getArticlesWithBestDiscount', (error, result) => {
    this.state.set('articlesWithBestDiscount', result);
  });
});

Template.MostOrdered.onCreated(function () {
  this.state = new ReactiveDict();
  Meteor.call('order.getMostOrderedArticles', (error, result) => {
    this.state.set('mostOrderedArticles', result);
  });
});

Template.BestPromotions.helpers({
  articlesWithBestDiscountCount() {
    const articles = Template.instance().state.get('articlesWithBestDiscount');
    return articles ? articles.length : false;
  },
  articlesWithBestDiscount() {
    return Template.instance().state.get('articlesWithBestDiscount');
  },
});

Template.MostOrdered.helpers({
  mostOrderedArticlesCount() {
    const articles = Template.instance().state.get('mostOrderedArticles');
    return articles ? articles.length : false;
  },
  mostOrderedArticles() {
    return Template.instance().state.get('mostOrderedArticles');
  },
});
