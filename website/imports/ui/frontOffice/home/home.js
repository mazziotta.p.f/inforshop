import './templates/home.html';
import './templates/presentation.html';
import './style/home.css';
import './style/home.mobile.css';

import './home.controller.js';

import './home.helpers.js';
import './home.routes.js';
