
FrontOfficeController = RouteController.extend({
  layoutTemplate: 'FrontOfficeLayout',
  yieldRegions: {
    'FrontOfficeUserMenu': {to: 'user'},
    'FrontOfficeAside': {to: 'aside'},
    'FrontOfficeHeader': {to: 'header'},
    'FrontOfficeFooter': {to: 'footer'},
    'FrontOfficeNav': {to: 'nav'},
    'ArticlesSearch': {to: 'search'},
  },
  subscriptions: function() {
    Meteor.subscribe('generalConfigurationAddress');
  },
  data: {
    information: function() {
      return GeneralConfiguration.findOne();
    },
  },
  protected: false,
  //notFoundTemplate: '404NotFound',
  //loadingTemplate: 'Loading',
});
