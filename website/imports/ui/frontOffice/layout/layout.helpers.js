
import { Brands } from '/imports/api/brands/brands.js';
import { Articles } from '/imports/api/articles/articles.js';
import { Categories } from '/imports/api/categories/categories.js';
import { GeneralConfiguration } from '/imports/api/generalConfiguration/generalConfiguration.js';

Template.registerHelper( 'formattedPrice', (value) => Formatter.price(value));
Template.registerHelper( 'formattedPercentage', (value) => Formatter.percentage(value));
Template.registerHelper( 'formattedDate', (value) => Formatter.date(value));

Template.FrontOfficeUserMenu.helpers({
  userDenomination: function() {
    if (Meteor.user()){
      let profile = Meteor.user().profile;
      if (profile && profile.name) return profile.name;
      else return Meteor.user().emails[0].address;
    }
  },
  isAuthorizedToAccessBackOffice: () => Roles.userIsInRole(
    Meteor.userId(), ['employee'], 'infoRshop'),
  articlesInCart: () => Cart.quantity,
});

Template.FrontOfficeNav.helpers({
  navEntries: function() {
    return [{
      navEntry : 'Articles',
      routeName : 'articles',
      routeNamePrefix : 'article',
    }];
  },
});

Template.FrontOfficeNavEntry.helpers({
  isCurrentRoute: function() {
    return Router.current().route.getName().startsWith(this.routeNamePrefix);
  },
});

Template.FrontOfficeAsideCategory.helpers({
  brands: function() {
    if (this.slug === Router.current().params.categorySlug){
      const categorySlug = Router.current().params.categorySlug;
      const category = Categories.findOne({slug: categorySlug});
      return findBrands(category);
    } else {
      try {
        const article = Router.current().data().article();
        if (this._id === article.categoryId) {
          const category = Categories.findOne({_id: article.categoryId})
          return findBrands(category);
        }
      } catch (e) {
        return;
      }
    }
  },
  categorySlug: function() {
    return Template.parentData().slug;
  },
  isCurrentCategory: function() {
    // current context is the current Category
    if (this.slug === Router.current().params.categorySlug) {
      return true;
    } else {
      try {
        const article = Router.current().data().article();
        return article && this._id === article.categoryId;
      } catch (e) {
        return false;
      }
    }
    return false;
  },
  isCurrentBrand: function() {
    // current context is the current Brand
    if (this.slug === Router.current().params.brandSlug) {
      return true;
    } else {
      try {
        const article = Router.current().data().article();
        return article && this._id === article.brandId;
      } catch (e) {
        return false;
      }
    }
    return false;
  },
});

Template.FrontOfficeFooter.helpers({
  information(){
    // general information to display in the footer
    return GeneralConfiguration.findOne();
  },
});

function findBrands(category) {
  const articlesBrandIds = Articles.find({categoryId: category._id})
    .map(function(article) {
      return article.brandId;
    }
  );
  return Brands.find({_id: {$in: articlesBrandIds}}, {sort: {slug: 1}});
}
