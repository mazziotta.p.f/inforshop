
Template.LoadingBackOffice.onCreated(function() {
  document.redirectHomeAtTimeout = setTimeout(function() {
    Router.go('home', {replaceState: true});
  }, 5000);
});

Template.FrontOfficeUserMenu.events({
  'click [data-view-action="display-account-form"]'(event, template) {
    template.find('.account-form').style.display = 'block';
  },
  'click [data-view-action="close-account-form"]'(event, template) {
    template.find('.account-form').style.display = 'none';
  },
  'click [data-view-action="display-cart-manager"]'(event, template) {
    let element = template.find('.cart-manager');
    if (element) element.style.display = 'block';
  },
});
