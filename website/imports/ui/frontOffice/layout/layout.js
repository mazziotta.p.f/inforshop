import './templates/components/user.html';
import './templates/components/aside.html';
import './templates/components/header.html';
import './templates/components/footer.html';
import './templates/components/nav.html';
import './templates/components/loading.html';
import './templates/errors/404NotFound.html';

import './templates/layout.html';

import './style/default.css';
import './style/mobile.css';

import './layout.events.js';
import './layout.helpers.js';
