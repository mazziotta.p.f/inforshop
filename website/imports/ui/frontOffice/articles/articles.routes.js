
import { Articles } from '/imports/api/articles/articles.js';
import { Images } from '/imports/api/images/images.js';
import { Categories } from '/imports/api/categories/categories.js';
import { Brands } from '/imports/api/brands/brands.js';
import { Promotions } from '/imports/api/promotions/promotions.js';

Router.route('/articles', {
  controller: 'FrontOfficeController',
  name: 'articles',
  template: 'ArticlesList',
  subscriptions: function() {
    Meteor.subscribe('lightArticles');
    Meteor.subscribe('images');
    Meteor.subscribe('categories');
    Meteor.subscribe('currentPromotions');
  },
  data: {
    categories: () => Categories.find({}, {sort: {slug: 1}}),
    articles: () => Articles.find(),
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ articles';
  },
});

Router.route('/articles/:categorySlug', {
  controller: 'FrontOfficeController',
  name: 'articlesByCategory',
  template: 'ArticlesList',
  subscriptions: function() {
    Meteor.subscribe('lightArticles');
    Meteor.subscribe('images');
    Meteor.subscribe('categories');
    Meteor.subscribe('brands');
    Meteor.subscribe('currentPromotions');
  },
  data: {
    categories: () => Categories.find({}, {sort: {slug: 1}}),
    articles: function() {
      const categorySlug = Router.current().params.categorySlug;
      const category = Categories.findOne({slug: categorySlug})
      if (category) {
        return Articles.find({categoryId: category._id});
      }
    },
  },
  onAfterAction: function() {
    let category = Categories.findOne({slug: Router.current().params.categorySlug});
    if (category)
      document.title = 'infoRshop ~ articles > ' + category.name;
  },
});

Router.route('/articles/:categorySlug/:brandSlug', {
  controller: 'FrontOfficeController',
  name: 'articlesByCategoryAndBrand',
  template: 'ArticlesList',
  subscriptions: function() {
    Meteor.subscribe('lightArticles');
    Meteor.subscribe('images');
    Meteor.subscribe('categories');
    Meteor.subscribe('brands');
    Meteor.subscribe('currentPromotions');
  },
  data: {
    categories: () => Categories.find({}, {sort: {slug: 1}}),
    articles: function() {
      const categorySlug = Router.current().params.categorySlug;
      const category = Categories.findOne({slug: categorySlug})
      const brandSlug = Router.current().params.brandSlug;
      const brand = Brands.findOne({slug: brandSlug});
      if (category && brand) {
        return Articles.find({categoryId: category._id, brandId: brand._id});
      }
    },
  },
  onAfterAction: function() {
    let category = Categories.findOne({slug: Router.current().params.categorySlug});
    let brand = Brands.findOne({slug: Router.current().params.brandSlug});
    if (brand && category)
      document.title = 'infoRshop ~ articles > ' + category.name + ' > ' + brand.name;
  },
});

Router.route('/article/permalink/:articleId', {
  controller: 'FrontOfficeController',
  name: 'articlePermalink',
  template: 'ArticleDetails',
  subscriptions: function() {
    Meteor.subscribe('articleById', Router.current().params.articleId);
    Meteor.subscribe('images');
    Meteor.subscribe('categories');
    Meteor.subscribe('brands');
    Meteor.subscribe('currentPromotions');
  },
  data: {
    article: () => Articles.findOne({_id: Router.current().params.articleId}),
    categories: () => Categories.find({}, {sort: {slug: 1}}),
    image: function() {
      const article = Router.current().data().article();
      if (article) return Images.findOne({name: article.imageName});
    },
  },
  onAfterAction: function() {
    document.title = 'infoRshop ~ article > ' + Router.current().params._id;
  },
});
// Route to article through slug
Router.route('/article/:slug', {
  controller: 'FrontOfficeController',
  name: 'articleSlug',
  template: 'ArticleDetails',
  subscriptions: function() {
    Meteor.subscribe('articleBySlug', Router.current().params.slug);
    Meteor.subscribe('images');
    Meteor.subscribe('categories');
    Meteor.subscribe('brands');
    Meteor.subscribe('currentPromotions');
  },
  data: {
    categories: () => Categories.find({}, {sort: {slug: 1}}),
    article: () => Articles.findOne({slug: Router.current().params.slug}),
    image: function() {
      const article = Router.current().data().article();
      if (article) {
        return Images.findOne({name: article.imageName});
      }
    },
  },
  onAfterAction: function() {
    const article = Router.current().data().article()
    if (article) document.title = 'infoRshop ~ article > ' + article.name;
  },
});
