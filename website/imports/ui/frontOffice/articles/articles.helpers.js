
import { Images } from '/imports/api/images/images.js';
import { Promotions } from '/imports/api/promotions/promotions.js';

Template.ArticlesListElement.helpers({
  image() {
    return Images.findOne({name: this.imageName});
  },
  quantityInCart() {
    if (Cart.entries) {
      let found = Cart.entries.find((entry) => {
        if(entry.articleId === this._id) return entry;
      });
      if (found) return found.quantity;
    }
    return 0;
  },
  hasPromotions() {
    return Promotions.find({articleId: this._id}).count() > 0;
  },
  totalDiscount() {
    let promotions = Promotions.find({articleId: this._id});
    return (Calculator.calculateCumulativeDiscount(promotions) * 100).toFixed(0);
  },
  discountedPrice() {
    let promotions = Promotions.find({articleId: this._id});
    let discount = Calculator.calculateCumulativeDiscount(promotions);
    return (this.price * (1 - discount)).toFixed(2);
  },
});

Template.ArticleDetails.helpers({
  quantityInCart() {
    if (Cart.entries) {
      let found = Cart.entries.find((entry) => {
        if(entry.articleId === this.article()._id) return entry;
      });
      if (found) return found.quantity;
    }
    return 0;
  },
  hasPromotions() {
    return Promotions.find({articleId: this.article()._id}).count() > 0;
  },
  totalDiscount() {
    let promotions = Promotions.find({articleId: this.article()._id});
    return (Calculator.calculateCumulativeDiscount(promotions) * 100).toFixed(0);
  },
  discountedPrice() {
    let promotions = Promotions.find({articleId: this.article()._id});
    let discount = Calculator.calculateCumulativeDiscount(promotions);
    return (this.article().price * (1 - discount)).toFixed(2);
  },
});
