
Template.ArticlesListElement.events({
  'click [data-action="add-to-cart"]'(event, template) {
    Cart.add(this._id, 1);
  },
});

Template.ArticleDetails.events({
  'click [data-action="add-to-cart"]'(event, template) {
    Cart.add(template.data.article()._id, 1);
  },
});
