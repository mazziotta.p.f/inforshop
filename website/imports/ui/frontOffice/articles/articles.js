
import './style/articles.css';
import './style/articles.mobile.css';

import './templates/list.html';
import './templates/detail.html';

import './articles.helpers.js';
import './articles.events.js';

import './articles.routes.js';
