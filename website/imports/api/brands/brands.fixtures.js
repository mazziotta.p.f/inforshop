import { Brands } from './brands.js';

Meteor.startup(() => {
  if (Brands.find().count() === 0) {
    Meteor.call('brands.create', 'HP');
    Meteor.call('brands.create', 'Acer');
    Meteor.call('brands.create', 'Asus');
    Meteor.call('brands.create', 'Samsung');
    Meteor.call('brands.create', 'megaport.fr');
    Meteor.call('brands.create', 'EMPIRE GAMING');
  }
});
