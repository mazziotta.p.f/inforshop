
import { Meteor } from 'meteor/meteor';

import { Brands } from '/imports/api/brands/brands.js';

Meteor.publish('brands', function () {
  return Brands.find();
});
