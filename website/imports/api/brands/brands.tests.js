import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { Brands } from '/imports/api/brands/brands.js';
import { Articles } from '/imports/api/articles/articles.js';

import '/imports/api/brands/brands.methods.js';

if (Meteor.isServer) {
  describe('Brands', () => {
    describe('methods', () => {
      let name = "testBrand";
      let brandId;

      beforeEach(() => {
        Articles.insert({
          name : "azerty",
          description : "qsdfgh",
          modelNumber : "wxcvbn",
          categoryName : "poiuy",
          price : 1,
          supplierDiscount : 0,
          brandId : "aaa",
        });


        Brands.remove({});
        brandId = Brands.insert({
          slug: slugify("BrandTest"),
          name: "BrandTest",
          bindableValues: ["BrandTest", "bindableToBeRemoved"]
        });
      });
      afterEach(() => {
        Articles.remove({});
        Brands.remove({});
      });

      it('can create brand', () => {
        const addBrand = Meteor.server.method_handlers['brands.create'];

        const invocation = {};

        addBrand.apply(invocation, [name]);

        assert.equal(Brands.find().count(), 2);
      });
      it('can remove brand', () => {
        const removeBrand = Meteor.server.method_handlers['brands.remove'];

        const invocation = {};

        removeBrand.apply(invocation, [brandId]);

        assert.equal(Brands.find().count(), 0);
      });
      it('can add bindable value', () => {
        const addBrandBindableValue = Meteor.server.method_handlers['brands.addBindableValue'];

        const invocation = {};

        addBrandBindableValue.apply(invocation, [brandId, "bidule"]);

        assert.equal(Brands.find({_id: brandId, bindableValues: "bidule"}).count(), 1);
      });
      it('can remove bindable value', () => {
        const removeBrandBindableValue = Meteor.server.method_handlers['brands.removeBindableValue'];

        const invocation = {};

        assert.equal(Brands.find({_id: brandId, bindableValues: "bindableToBeRemoved"}).count(), 1)
        removeBrandBindableValue.apply(invocation, [brandId, "bindableToBeRemoved"]);
        assert.equal(Brands.find({_id: brandId, bindableValues: "bindableToBeRemoved"}).count(), 0)
      });
    });
  });
}
