
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Brands } from '/imports/api/brands/brands.js';
import { Articles } from '/imports/api/articles/articles.js';

Meteor.methods({
  'brands.create'(name) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(name, String);

    Brands.insert({
      slug: slugify(name),
      name,
      bindableValues: [name]
    });
  },
  'brands.remove'(brandId) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(brandId, String);

    if (Articles.find({brandId}).count()) {
       throw new Meteor.Error('brand-not-empty');
    }

    Brands.remove({_id: brandId});
  },
  'brands.addBindableValue'(brandId, value) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(brandId, String);
    check(value, String);

    Brands.update(
      {_id: brandId},
      {$push: {bindableValues: value}}
    );
  },
  'brands.removeBindableValue'(brandId, value) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(brandId, String);
    check(value, String);

    Brands.update(
      {_id: brandId},
      {$pull: {bindableValues: value}}
    );
  },
});
