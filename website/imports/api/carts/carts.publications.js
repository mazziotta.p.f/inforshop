
import { Meteor } from 'meteor/meteor';

import { Carts } from '/imports/api/carts/carts.js';

Meteor.publish('carts', function () {
  if (Meteor.userId()) return Carts.find({owner: Meteor.userId()});
});
