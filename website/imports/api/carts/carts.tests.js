import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { assert } from 'meteor/practicalmeteor:chai';
import { sinon } from 'meteor/practicalmeteor:sinon';

import { Carts } from '/imports/api/carts/carts.js';
import { Articles } from '/imports/api/articles/articles.js';

import '/imports/api/carts/carts.methods.js';

function randomInt (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

if (Meteor.isServer) {
  describe('Carts', () => {
    describe('methods', () => {
      let commonUser;

      beforeEach(() => {
        commonUser = Accounts.createUser({
          email: 'common@testing',
          password: '123456',
          username: 'commonUser',
        });

        Articles.remove({});

        articleId = Articles.insert({
          name : "azerty",
          description : "qsdfgh",
          modelNumber : "wxcvbn",
          categoryName : "poiuy",
          price : 1,
          advisedPrice: 2,
          supplierDiscount : 0,
          brandId : "aaa",
          isAvailable: true,
          restockingTime: 42,
          color: "Yellow",
          slug: "slug of Test Article",
          technicals: [
            {
            name : "GPU",
			      value : "Intel"
            },
          ],
        });
        Carts.insert({
          owner: commonUser,
          entries: [],
        });
      });
      afterEach(() => {
        //Security.denyIfNotConnected.restore();
        Articles.remove({});
        Carts.remove({});
        Meteor.users.remove({});
      });
      it('can add article', () => {

        let quantity = randomInt(1,50);

        const addArticleToCart = Meteor.server.method_handlers['carts.addArticle'];

        const invocation = {userId: commonUser};

        addArticleToCart.apply(invocation, [articleId, quantity]);

        assert.equal(Carts.find({owner: commonUser, entries: {$elemMatch: {articleId, quantity}}}).count(), 1);
      });
      it('can update number of article', () => {
        let quantity = randomInt(1,10);
        let secondQuantity = randomInt(11,20);
        let totalQuantity = quantity + secondQuantity;

        const addArticleToCart = Meteor.server.method_handlers['carts.addArticle'];

        const invocation = {userId: commonUser};

        addArticleToCart.apply(invocation, [articleId, quantity]);
        addArticleToCart.apply(invocation, [articleId, secondQuantity]);

        assert.equal(Carts.find({owner: commonUser, entries: {$elemMatch: {articleId, quantity: totalQuantity}}}).count(), 1);
      });
    });
  });
}
