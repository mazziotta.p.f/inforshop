
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Carts } from '/imports/api/carts/carts.js';
import { Articles } from '/imports/api/articles/articles.js';


const MAXIMUM_ARTICLE_ORDER_QUANTITY = 50;

Meteor.methods({
  'carts.addArticle' (articleId, quantity) {

    if (this.connection) Security.denyIfNotConnected();

    check(articleId, String);
    check(quantity, Number);

    if(quantity > MAXIMUM_ARTICLE_ORDER_QUANTITY)
      throw new Meteor.Error(
        401, 'Error 401: Article quantity cannot exceed ' +
        MAXIMUM_ARTICLE_ORDER_QUANTITY);

    const article = Articles.findOne({_id: articleId});

    if (!article)
      throw new Meteor.Error(404, 'Error 404: Article could not be found');

    const cart = Carts.findOne({owner: this.userId});

    if (cart) {

      const articleFromCart = findArticleInCart(article, cart);

      if (articleFromCart)
        updateEntryQuantity(this.userId, articleFromCart, quantity)
      else
        addEntry(this.userId, article, quantity)
    }
    else
      createCartWithEntry(this.userId, article, quantity)
  },

  'carts.removeArticle' (articleId) {
    if (this.connection) Security.denyIfNotConnected();

    const article = Articles.findOne({_id: articleId});

    if (!article)
      throw new Meteor.Error(404, 'Error 404: Article could not be found');

    Carts.update(
      {owner: this.userId},
      {$pull: {entries: {articleId}}}
    );
  },
  'carts.clear' () {
    if (this.connection) Security.denyIfNotConnected();

    Carts.update(
      {owner: this.userId},
      {$set: {entries: []}}
    );
  },
});

function updateEntryQuantity(owner, article, quantity) {
  const newQuantity = article.quantity + quantity;
  
  if(newQuantity < 1)
    throw new Meteor.Error(
      401, 'Error 401: Article quantity cannot be less than 1');
  else if (newQuantity > MAXIMUM_ARTICLE_ORDER_QUANTITY)
    throw new Meteor.Error(
      401, 'Error 401: Article quantity cannot exceed ' +
        MAXIMUM_ARTICLE_ORDER_QUANTITY);

  Carts.update(
    {owner, 'entries.articleId': article.articleId},
    {$set: {'entries.$.quantity': newQuantity}}
  );
}

function findArticleInCart(article, cart) {
  return cart.entries.find((entry) => {
    if (entry.articleId === article._id) return entry;
  });
}

function addEntry(owner, article, quantity) {
  if(quantity < 1)
    throw new Meteor.Error(
      401, 'Error 401: Article quantity cannot be less than 1');
  Carts.update(
    {owner},
    {$push: {entries: {articleId: article._id, slug: article.slug, quantity}}}
  );
}

function createCartWithEntry(owner, article, quantity) {
  Carts.insert({
    owner,
    entries: [{
      articleId: article._id,
      slug: article.slug,
      quantity
    }]}
  );
}
