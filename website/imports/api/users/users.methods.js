
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

Meteor.methods({
  'users.createEmployee'(email, password) {

    if (this.connection) Security.check(['admin']);

    check(email, String);
    check(password, String);

    Roles.addUsersToRoles(Accounts.createUser({email, password}), 'employee', 'infoRshop');
  },
  'users.removeEmployee'(userId) {

    if (this.connection) Security.check(['admin']);

    check(userId, String);

    Meteor.users.remove({_id: userId, 'roles.infoRshop': 'employee'});
  },
  'users.toggleAdminRole'(userId, makeActive) {

    if (this.connection) Security.check(['admin']);

    check(userId, String);
    check(makeActive, Boolean);

    if (!makeActive && !Meteor.users.find(
      {_id: {$nin: [userId]},
      'roles.infoRshop': 'admin'}).count()
    )
      throw new Meteor.Error(500, 'Error 500: admin role unassignation could not be done',
        'If this admin role assignation gets removed, everybody would be locked out of user management.');

    if (makeActive) Roles.addUsersToRoles(userId, 'admin', 'infoRshop');
    else Roles.removeUsersFromRoles(userId, 'admin', 'infoRshop');
  },
  'users.toggleManagerRole'(userId, makeActive) {

    if (this.connection) Security.check(['admin']);

    check(userId, String);
    check(makeActive, Boolean);

    if (makeActive) Roles.addUsersToRoles(userId, 'manager', 'infoRshop');
    else Roles.removeUsersFromRoles(userId, 'manager', 'infoRshop');
  },
  'users.toggleCommercialRole'(userId, makeActive) {

    if (this.connection) Security.check(['admin']);

    check(userId, String);
    check(makeActive, Boolean);

    if (makeActive) Roles.addUsersToRoles(userId, 'commercial', 'infoRshop');
    else Roles.removeUsersFromRoles(userId, 'commercial', 'infoRshop');
  },
});
