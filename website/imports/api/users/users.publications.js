
import { Meteor } from 'meteor/meteor';

Meteor.publish('employees', function(){
  if (Security.isAllowed(['admin']))
    return Meteor.users.find({'roles.infoRshop': 'employee'});
});
