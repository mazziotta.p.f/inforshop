
if (Meteor.isServer) {
  Meteor.startup(() => {
    if ( Meteor.users.find().count() === 0 ) {
      Accounts.createUser({
        email: 'user@shop.info',
        password: '123456',
      });
      let adminId = Accounts.createUser({
        email: 'admin@shop.info',
        password: '123456',
      });
      let managerId = Accounts.createUser({
        email: 'manager@shop.info',
        password: '123456',
      });
      let commercialId = Accounts.createUser({
        email: 'commercial@shop.info',
        password: '123456',
      });
      Roles.addUsersToRoles(adminId, ['admin', 'employee'], 'infoRshop');
      Roles.addUsersToRoles(managerId, ['manager', 'employee'], 'infoRshop');
      Roles.addUsersToRoles(commercialId, ['commercial', 'employee'], 'infoRshop');
    }
  });
}
