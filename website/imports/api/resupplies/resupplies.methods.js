
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Resupplies } from '/imports/api/resupplies/resupplies.js';
import { Articles } from '/imports/api/articles/articles.js';

Meteor.methods({
  'resupplies.create'(){

    if (this.connection) Security.check(['admin', 'manager']);

    Resupplies.insert({
      createdAt: new Date(),
      isActive: true,
      articles: [],
    });
  },
  'resupplies.addArticle' (articleId){

    if (this.connection) Security.check(['admin', 'manager']);

    check(articleId, String);

    const article = Articles.findOne({_id: articleId });
    if (!article)
      throw new Meteor.Error(404, 'Error 404: There is not article with this id.');

    const resupply = Resupplies.findOne({isActive: true});
    if (!resupply)
      throw new Meteor.Error(404, 'Error 404: There is not resupply active.');

    let articleFound = resupply.articles.find(
      (article) => { if (article.articleId === articleId) return true; }
    );

    if (articleFound)
      throw new Meteor.Error(500, 'Error 500: Article already present');

    Resupplies.update(
      {_id: resupply._id},
      {
        $push: { articles: {
          articleId,
          quantity: 0,
        }}
      }
    );
  },
  'resupplies.updateArticle' (articleId, quantity){

    if (this.connection) Security.check(['admin', 'manager']);

    check(articleId, String);
    check(quantity, Number);

    const article = Articles.findOne({_id: articleId });
    if (!article)
      throw new Meteor.Error(404, 'Error 404: There is not article with this id.');

    const resupply = Resupplies.findOne({isActive: true});
    if (!resupply)
      throw new Meteor.Error(404, 'Error 404: There is not resupply active.');

    if (quantity < 0)
      throw new Meteor.Error(500, 'Error 500: observed quantity cannot be below zero.');

    Resupplies.update(
      {
        _id: resupply._id,
        "articles.articleId": articleId
      },
      {
        $set: {'articles.$.quantity': quantity}
      }
    );
  },
  'resupplies.removeArticle' (articleId){

    if (this.connection) Security.check(['admin', 'manager']);

    check(articleId, String);

    const article = Articles.findOne({_id: articleId });
    if (!article)
      throw new Meteor.Error(404, 'Error 404: There is not article with this id.');

    const resupply = Resupplies.findOne({isActive: true});
    if (!resupply)
      throw new Meteor.Error(404, 'Error 404: There is not resupply active.');

    Resupplies.update(
      {_id: resupply._id},
      {
        $pull: {'articles': {articleId}}
      }
    );
  },
  'resupplies.apply'() {

    if (this.connection) Security.check(['admin', 'manager']);

    const resupply = Resupplies.findOne({isActive: true});
    if (!resupply)
      throw new Meteor.Error(404, 'Error 404: There is not resupply active.');

    resupply.articles.forEach((article) => {
      Articles.update({_id: article.articleId}, {$inc: {stock: article.quantity}});
    });

    Resupplies.update({_id: resupply._id}, {$set: {isActive: false}});
  },
});
