import { Meteor } from 'meteor/meteor';

import { Resupplies } from '/imports/api/resupplies/resupplies.js';

Meteor.publish('resupplies', () => {
  if (Security.isAllowed(['admin', 'manager']))
  return Resupplies.find({isActive: true}, {sort: {createdAt: -1}, limit: 1});
});
