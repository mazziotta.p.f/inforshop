import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { Resupplies } from '/imports/api/resupplies/resupplies.js';
import { Articles } from '/imports/api/articles/articles.js';

import '/imports/api/resupplies/resupplies.methods.js';

if (Meteor.isServer) {
  describe('Resupplies', () => {
    describe('methods', () => {
      let articleId;
      beforeEach(() => {
        Articles.remove({});
        Resupplies.remove({});

        articleId = Articles.insert({
          name : "azerty",
          description : "qsdfgh",
          modelNumber : "wxcvbn",
          categoryName : "poiuy",
          price : 1,
          advisedPrice: 2,
          supplierDiscount : 0,
          brandId : "aaa",
          isAvailable: true,
          restockingTime: 42,
          color: "Yellow",
          technicals: [
            {
            name : "GPU",
			      value : "Intel"
            },
          ],
        });

        Resupplies.insert({
          createdAt: new Date(),
          isActive: true,
          articles: [
            {
              articleId,
              quantity: 11
            }
          ]
        });

      });
      afterEach(() => {
        Articles.remove({});
        Resupplies.remove({});
      });
      it('can create new resupply', () =>{
        const createResupply = Meteor.server.method_handlers['resupplies.create'];

        const invocation = {};

        createResupply.apply(invocation, []);
        assert.equal(Resupplies.find({isActive:true}).count(), 2);
      });
      it('can add article', () => {
        let newArticleId = Articles.insert({
          name : "azerty",
          description : "qsdfgh",
          modelNumber : "wxcvbn",
          categoryName : "poiuy",
          price : 1,
          advisedPrice: 2,
          supplierDiscount : 0,
          brandId : "aaa",
          isAvailable: true,
          restockingTime: 42,
          color: "Yellow",
          technicals: [
            {
            name : "GPU",
			      value : "Intel"
            },
          ],
        });

        const addArticleToResupply = Meteor.server.method_handlers['resupplies.addArticle'];

        const invocation = {};

        addArticleToResupply.apply(invocation, [newArticleId]);
        assert.equal(Resupplies.find({isActive: true, articles: {$elemMatch: {articleId: newArticleId}}}).count(), 1);
      });
      it('can update article', () => {
        let resupplyQuantity = 7;

        const updateArticleResupply = Meteor.server.method_handlers['resupplies.updateArticle'];

        const invocation = {};

        updateArticleResupply.apply(invocation, [articleId, resupplyQuantity]);

        assert.equal(Resupplies.find({isActive: true, articles: {$elemMatch: {articleId, quantity: resupplyQuantity}}}).count(), 1);
      });
      it('can remove article', () => {
        const removeArticleResupply = Meteor.server.method_handlers['resupplies.removeArticle'];

        const invocation = {};

        assert.equal(Resupplies.find({isActive: true, articles: {$elemMatch: {articleId}}}).count(), 1);
        removeArticleResupply.apply(invocation, [articleId]);
        assert.equal(Resupplies.find({isActive: true, articles: {$elemMatch: {articleId}}}).count(), 0);

      });
      it('can apply resupply', () => {
        const applyResupply = Meteor.server.method_handlers['resupplies.apply'];

        const invocation = {};

        applyResupply.apply(invocation, []);

        assert.equal(Resupplies.find({isActive: true}).count(), 0);
      });


    });
  });
}
