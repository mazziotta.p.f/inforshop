
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { GeneralConfiguration } from '/imports/api/generalConfiguration/generalConfiguration.js';

Meteor.methods({
  'generalConfiguration.reset' (){

    if (this.connection) Security.check(['admin']);

    let country       = "Belgium";
    let vat           = 21;
    let defaultMargin = 10;
    let street        = "Rue St Laurent 29";
    let postCode      = "4000";
    let city          = "Liège";

    GeneralConfiguration.remove({});
    GeneralConfiguration.insert({
      country,
      vat,
      defaultMargin,
      street,
      postCode,
      city,
      deliveryFees: [
        {
          minimumDistance: 0,
          fee: 0
        },{
          minimumDistance: 5,
          fee: 10
        },{
          minimumDistance: 10,
          fee: 15
        },{
          minimumDistance: 20,
          fee: 20
        },
      ]
    });
    Meteor.call('articles.recalculatePrices');
  },
  'generalConfiguration.VAT.update'(value) {

    if (this.connection) Security.check(['admin']);

    check(value, Number);

    if (GeneralConfiguration.findOne({})) {
      GeneralConfiguration.update(
        {country: 'Belgium'},
        {$set: {vat: value}});
        Meteor.call('articles.recalculatePrices');
    }
    else {
      throw new Meteor.Error('No VAT !');
    }
  },
  'generalConfiguration.defaultMargin.update'(value) {

    if (this.connection) Security.check(['admin']);

    check(value, Number);

    if (GeneralConfiguration.findOne({})) {
      GeneralConfiguration.update(
        {country: 'Belgium'},
        {$set: {defaultMargin: value}});
        Meteor.call('articles.recalculatePrices');
    }
    else {
      throw new Meteor.Error('No default margin !');
    }
  },
  'generalConfiguration.defaultStreet.update'(value) {

    if (this.connection) Security.check(['admin']);

    check(value, String);

    if (GeneralConfiguration.findOne({})) {
      GeneralConfiguration.update(
        {country: 'Belgium'},
        {$set: {street: value}});
    }
    else {
      throw new Meteor.Error('No default street !');
    }
  },
  'generalConfiguration.defaultPostCode.update'(value) {

    if (this.connection) Security.check(['admin']);

    check(value, String);

    if (GeneralConfiguration.findOne({})) {
      GeneralConfiguration.update(
        {country: 'Belgium'},
        {$set: {postCode: value}});
    }
    else {
      throw new Meteor.Error('No default post code !');
    }
  },
  'generalConfiguration.defaultCity.update'(value) {

    if (this.connection) Security.check(['admin']);

    check(value, String);

    if (GeneralConfiguration.findOne({})) {
      GeneralConfiguration.update(
        {country: 'Belgium'},
        {$set: {city: value}});
    }
    else {
      throw new Meteor.Error('No default city !');
    }
  },
  'generalConfiguration.addDeliveryFee' (minimumDistance, fee) {

    if (this.connection) Security.check(['admin']);

    check(minimumDistance, Number);
    check(fee, Number);

    if (isNaN(minimumDistance) || isNaN(fee)) {
      throw new Meteor.Error('Could not parse numbers !');
    }

    if (GeneralConfiguration.findOne({country: 'Belgium', 'deliveryFees.minimumDistance': minimumDistance})) {
      throw new Meteor.Error('This delivery fee already exists !');
    }

    GeneralConfiguration.update(
      {country: 'Belgium'},
      {$push: {'deliveryFees': {minimumDistance, fee}}}
    );
  },
  'generalConfiguration.removeDeliveryFee' (minimumDistance) {

    if (this.connection) Security.check(['admin']);

    check(minimumDistance, Number);

    GeneralConfiguration.update(
      {country: 'Belgium'},
      {$pull: {
        'deliveryFees': { minimumDistance }
      }
    });
  },
  'generalConfiguration.updateDeliveryFee' (minimumDistance, fee) {

    if (this.connection) Security.check(['admin']);

    check(minimumDistance, Number);
    check(fee, Number);

    GeneralConfiguration.update(
      {country: 'Belgium', 'deliveryFees.minimumDistance': minimumDistance},
      {$set: {
        'deliveryFees.$.fee': fee
      }
    });
  },
  'generalConfiguration.getServerAddress'() {
    const config = GeneralConfiguration.findOne();
    return config.street + ', ' + config.postCode + ' ' + config.city;
  }
});
