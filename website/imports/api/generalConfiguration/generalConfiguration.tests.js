import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { GeneralConfiguration } from '/imports/api/generalConfiguration/generalConfiguration.js';

import '/imports/api/generalConfiguration/generalConfiguration.methods.js';

if (Meteor.isServer) {
  describe('General Configuration', () => {
    describe('methods', () => {
      beforeEach(() => {
        Meteor.call('generalConfiguration.reset')
      });
      afterEach(() => {
        GeneralConfiguration.remove({});
      });
      it('can update VAT', () => {
        let vat = 99;

        const updateVat = Meteor.server.method_handlers['generalConfiguration.VAT.update'];

        const invocation = {};

        updateVat.apply(invocation, [vat]);

        assert.equal(GeneralConfiguration.find({vat}).count(), 1);
      });
      it('can uupdate default margin', () => {
        let defaultMargin = 99;

        const updateVat = Meteor.server.method_handlers['generalConfiguration.defaultMargin.update'];

        const invocation = {};

        updateVat.apply(invocation, [defaultMargin]);

        assert.equal(GeneralConfiguration.find({defaultMargin}).count(), 1);
      });
      it('can upudate default street', () => {
        let street = "Rue bignoux";

        const updateVat = Meteor.server.method_handlers['generalConfiguration.defaultStreet.update'];

        const invocation = {};

        updateVat.apply(invocation, [street]);

        assert.equal(GeneralConfiguration.find({street}).count(), 1);
      });
      it('can upudate default post code', () => {
        let postCode = "1234";

        const updateVat = Meteor.server.method_handlers['generalConfiguration.defaultPostCode.update'];

        const invocation = {};

        updateVat.apply(invocation, [postCode]);

        assert.equal(GeneralConfiguration.find({postCode}).count(), 1);
      });
      it('can upudate default city', () => {
        let city = "TrouPerdu";

        const updateVat = Meteor.server.method_handlers['generalConfiguration.defaultCity.update'];

        const invocation = {};

        updateVat.apply(invocation, [city]);

        assert.equal(GeneralConfiguration.find({city}).count(), 1);
      });
      it('can add delivery fee', () => {
        let minimumDistance = 8;
        let fee = 9;

        const updateVat = Meteor.server.method_handlers['generalConfiguration.addDeliveryFee'];

        const invocation = {};

        updateVat.apply(invocation, [minimumDistance, fee]);

        assert.equal(GeneralConfiguration.find({deliveryFees: {$elemMatch: {minimumDistance, fee}}}).count(), 1);
      });
      it('can remove delivery fee', () => {
        let minimumDistance = 10;

        const updateVat = Meteor.server.method_handlers['generalConfiguration.removeDeliveryFee'];

        const invocation = {};

        assert.equal(GeneralConfiguration.find({deliveryFees: {$elemMatch: {minimumDistance}}}).count(), 1);
        updateVat.apply(invocation, [minimumDistance]);
        assert.equal(GeneralConfiguration.find({deliveryFees: {$elemMatch: {minimumDistance}}}).count(), 0);
      });
      it('can update delivery fee', () => {
        let minimumDistance = 10;
        let fee = 9;

        const updateVat = Meteor.server.method_handlers['generalConfiguration.updateDeliveryFee'];

        const invocation = {};

        updateVat.apply(invocation, [minimumDistance, fee]);

        assert.equal(GeneralConfiguration.find({deliveryFees: {$elemMatch: {minimumDistance, fee}}}).count(), 1);
      });
    });
  });
}
