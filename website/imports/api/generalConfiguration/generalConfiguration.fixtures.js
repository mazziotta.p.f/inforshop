import { GeneralConfiguration } from './generalConfiguration.js';

Meteor.startup(() =>{
  if (GeneralConfiguration.find().count() == 0) {
    Meteor.call('generalConfiguration.reset');
  }
});
