
import { Meteor } from 'meteor/meteor';

import { GeneralConfiguration } from '/imports/api/generalConfiguration/generalConfiguration.js';

Meteor.publish('generalConfiguration', () => {
  if (Security.isAllowed(['admin', 'manager', 'commercial']))
  return GeneralConfiguration.find();
});
Meteor.publish('generalConfigurationAddress', () => GeneralConfiguration.find(
    {},
    {
      fields: {
        street: 1,
        postCode: 1,
        city: 1,
        country: 1
      }
    }
  )
);
