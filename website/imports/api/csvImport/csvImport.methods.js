
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { CSVImportArticles } from '/imports/api/csvImport/csvImport.js';
import { Articles } from '/imports/api/articles/articles.js';
import { Categories } from '/imports/api/categories/categories.js';
import { Brands } from '/imports/api/brands/brands.js';
import { ArticleDefaults } from '/imports/api/articles/articleDefaults.js';

Meteor.methods({
  'csvImportArticles.parseUpload'(data) {

    if (this.connection) Security.check(['admin', 'manager']);

    Meteor.call('csvImportArticles.bundleCreate', data);
  },
  'csvImportArticles.importFromCSVArticles'(schema) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(schema, Object);

    CSVImportArticles.find().forEach(function(csvArticle) {
      const category = Categories.findOne({
        bindableValues: csvArticle[getHeaderValue(schema, 'category')]
      });
      const brand = Brands.findOne({
        bindableValues: csvArticle[getHeaderValue(schema, 'brand')]
      });
      if (category && brand) {
        let articleDefaults = ArticleDefaults.findOne({categoryId: category._id});
        if (articleDefaults) {
          let article = buildArticle(schema, csvArticle, category, brand, articleDefaults);
          if(article.name) {
            // Insert the article ONLY if it doesn't exist
            if (!Articles.findOne({slug: article.slug})) {
              let articleId = Articles.insert(article);
              Meteor.call('articles.recalculatePrice', articleId);
              CSVImportArticles.remove({_id: csvArticle._id});
            }
          }
        } else {
          console.log('No articleDefaults found');
        }
      } else {
        console.log('No category or brand found');
      }
    });
  },
  'csvImportArticles.bundleCreate'(articlesArray){

    if (this.connection) Security.check(['admin', 'manager']);

    check(articlesArray, Array);

    articlesArray.forEach(function(article){
      CSVImportArticles.insert(article);
    });
  },
  'csvImportArticles.remove'(csvArticleId) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(csvArticleId, String);

    CSVImportArticles.remove({_id: csvArticleId});
  },
  'csvImportArticles.update'(csvArticleId, updateObject) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(csvArticleId, String);
    check(updateObject, Object);

    CSVImportArticles.update({_id : csvArticleId}, {$set: updateObject});
  },
});

function getHeaderValue(schema, propertyName) {

  let result;

  Object.keys(schema).some(function(key) {
    if (schema[key] === propertyName) {
      result = key;
      return true;
    }
  });
  return result;
}

function buildArticle(schema, csvArticle, category, brand, articleDefaults) {

  let technicalPropertyStartString = 'technicals.';

  let article = {};
  article.technicals = [];

  Object.keys(schema).forEach(function(key) {
    if (schema[key]) {
      let propertyName = schema[key];
      if (propertyName.startsWith(technicalPropertyStartString)) {
        let technicalPropertyName = propertyName
          .slice(technicalPropertyStartString.length);
        let technicalPropertyValue = csvArticle[key];
        if(!technicalPropertyValue) {
          let articleTechnicalDefault = articleDefaults.technicals
            .find((element) => {
              return element.name === technicalPropertyName;
            });
          if (articleTechnicalDefault) {
            technicalPropertyValue = articleTechnicalDefault.value;
          }
        }
        article.technicals.push({
          name: technicalPropertyName,
          value: technicalPropertyValue,
        });
      } else {
        article[propertyName] = csvArticle[key];
      }
    }
  });

  article.categoryId = category._id;
  article.brandId = brand._id;
  article.category = category.name;
  article.brand = brand.name;
  article.advisedPrice = parseFloat(article.advisedPrice.replace(',', '.'));
  article.supplierDiscount = parseFloat(article.supplierDiscount.replace(',', '.'));
  article.restockingTime = parseFloat(article.restockingTime.replace(',', '.'));
  article.isAvailable = false;
  article.description = '';
  article.price = 0;
  article.stock = 0;

  createArticleSlugAndName(article, category.name, brand.name,
    article.modelNumber, article.series, article.color);

  return article;
}

function createArticleSlugAndName(
  article, categoryName, brandName, modelNumber, series, color) {

  article.name = categoryName + ' ' + brandName + ' ' + modelNumber;
  if (series) {
    article.name += ' ' + series;
  }
  if (color) {
    article.name += ' ' + color;
  }

  article.slug = slugify(article.name);
}
