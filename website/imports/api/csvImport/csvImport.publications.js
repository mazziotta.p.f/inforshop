
import { Meteor } from 'meteor/meteor';

import { CSVImportArticles } from '/imports/api/csvImport/csvImport.js';

Meteor.publish('csvImportArticles', () => {
  if (Security.isAllowed(['admin', 'manager']))
    return CSVImportArticles.find();
});
