import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { Inventories } from '/imports/api/inventories/inventories.js';
import { Articles } from '/imports/api/articles/articles.js';

import '/imports/api/inventories/inventories.methods.js';

if (Meteor.isServer) {
  describe('Invenrories', () => {
    describe('methods', () => {
      let articleId;
      beforeEach(() => {
        Articles.remove({});
        Inventories.remove({});

        articleId = Articles.insert({
          name : "azerty",
          description : "qsdfgh",
          modelNumber : "wxcvbn",
          categoryName : "poiuy",
          price : 1,
          advisedPrice: 2,
          supplierDiscount : 0,
          brandId : "aaa",
          isAvailable: true,
          restockingTime: 42,
          color: "Yellow",
          technicals: [
            {
            name : "GPU",
			      value : "Intel"
            },
          ],
        });
        Inventories.insert({
          createdAt: new Date(),
          isActive: true,
          articles: [
            {
              articleId,
              expectetQuantity: 20,
              observedQuantity: 40,
            }
          ],
        });

      });
      afterEach(() => {
        Articles.remove({});
        Inventories.remove({});
      });
      it('can create new inventory', () => {
        const createInventory = Meteor.server.method_handlers['inventories.create'];

        const invocation = {};

        createInventory.apply(invocation, []);

        assert.equal(Inventories.find().count(), 2);
      });
      it('can add article', () => {
        let newArticleId = Articles.insert({
          name : "azerty",
          description : "qsdfgh",
          modelNumber : "wxcvbn",
          categoryName : "poiuy",
          price : 1,
          advisedPrice: 2,
          supplierDiscount : 0,
          brandId : "aaa",
          isAvailable: true,
          restockingTime: 42,
          color: "Yellow",
          technicals: [
            {
            name : "GPU",
			      value : "Intel"
            },
          ],
        });
        const addArticleToInventory = Meteor.server.method_handlers['inventories.addArticle'];

        const invocation = {};

        addArticleToInventory.apply(invocation, [newArticleId]);
        assert.equal(Inventories.find({isActive: true, articles: {$elemMatch: {articleId: newArticleId}}}).count(), 1);
      });
      it('can update article', () => {
        let observedQuantity = 33;
        const updateArticleInventory = Meteor.server.method_handlers['inventories.updateArticle'];

        const invocation = {};

        updateArticleInventory.apply(invocation, [articleId, observedQuantity]);
        assert.equal(Inventories.find({isActive: true, articles: {$elemMatch: {articleId, observedQuantity}}}).count(), 1)
      });
      it('can remove article', () => {
        const removeArticleInventory = Meteor.server.method_handlers['inventories.removeArticle'];

        const invocation = {};

        assert.equal(Inventories.find({isActive: true, articles: {$elemMatch: {articleId}}}).count(), 1)
        removeArticleInventory.apply(invocation, [articleId]);
        assert.equal(Inventories.find({isActive: true, articles: {$elemMatch: {articleId}}}).count(), 0)

      });
      it('can apply inventory', () =>{
        const applyInventory = Meteor.server.method_handlers['inventories.apply'];

        const invocation = {};

        applyInventory.apply(invocation, []);
        assert.equal(Inventories.find({isActive: true}).count(), 0);
      });
    });
  });
}
