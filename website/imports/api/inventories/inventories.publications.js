
import { Meteor } from 'meteor/meteor';

import { Inventories } from '/imports/api/inventories/inventories.js';

Meteor.publish('inventories', function(){
  return Inventories.find({isActive: true}, {sort: {createdAt: -1}, limit: 1});
});
