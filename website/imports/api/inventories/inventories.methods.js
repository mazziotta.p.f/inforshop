import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Inventories } from '/imports/api/inventories/inventories.js';
import { Articles } from '/imports/api/articles/articles.js';

Meteor.methods({
  'inventories.create' (){
    Inventories.insert({
      createdAt: new Date(),
      isActive: true,
      articles: [],
    });
  },
  'inventories.addArticle' (articleId){

    if (this.connection) Security.check(['admin', 'manager']);

    check(articleId, String);

    const article = Articles.findOne({_id: articleId });
    if (!article)
      throw new Meteor.Error(404, 'Error 404: There is not article with this id.');

    const inventory = Inventories.findOne({isActive: true});
    if (!inventory)
      throw new Meteor.Error(404, 'Error 404: There is not inventory active.');

    let articleFound = inventory.articles.find(
      (article) => {if (article.articleId === articleId) return true;}
    );

    if (articleFound)
      throw new Meteor.Error(500, 'Error 500: Article already present');

    Inventories.update(
      {_id: inventory._id},
      {
        $push: { articles: {
          articleId,
          expectedQuantity: article.stock,
          observedQuantity: article.stock,
        }}
      }
    );
  },
  'inventories.updateArticle' (articleId, observedQuantity){

    if (this.connection) Security.check(['admin', 'manager']);

    check(articleId, String);
    check(observedQuantity, Number);

    const article = Articles.findOne({_id: articleId });
    if (!article)
      throw new Meteor.Error(404, 'Error 404: There is not article with this id.');

    const inventory = Inventories.findOne({isActive: true});
    if (!inventory)
      throw new Meteor.Error(404, 'Error 404: There is not inventory active.');

    if (observedQuantity < 0)
      throw new Meteor.Error(500, 'Error 500: observed quantity cannot be below zero.');

    Inventories.update(
      {
        _id: inventory._id,
        "articles.articleId": articleId
      },
      {
        $set: {'articles.$.observedQuantity': observedQuantity}
      }
    );
  },
  'inventories.removeArticle' (articleId){

    if (this.connection) Security.check(['admin', 'manager']);

    check(articleId, String);

    const article = Articles.findOne({_id: articleId });
    if (!article)
      throw new Meteor.Error(404, 'Error 404: There is not article with this id.');

    const inventory = Inventories.findOne({isActive: true});
    if (!inventory)
      throw new Meteor.Error(404, 'Error 404: There is not inventory active.');

    Inventories.update(
      {_id: inventory._id},
      {
        $pull: {'articles': {articleId}}
      }
    );
  },
  'inventories.apply'() {
    if (this.connection) Security.check(['admin', 'manager']);

    const inventory = Inventories.findOne({isActive: true});
    if (!inventory)
      throw new Meteor.Error(404, 'Error 404: There is not inventory active.');

    inventory.articles.forEach((article) => {
      Articles.update({_id: article.articleId}, {$set: {stock: article.observedQuantity}});
    });

    Inventories.update({_id: inventory._id}, {$set: {isActive: false}});
  },
});
