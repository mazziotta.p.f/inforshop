
import { Meteor } from 'meteor/meteor';

import { Promotions } from '/imports/api/promotions/promotions.js';

Meteor.publish('promotions', function () {
  if (Security.isAllowed(['admin', 'manager', 'commercial']))
    return Promotions.find();
});
Meteor.publish('articlePromotions', function (articleId) {
  return Promotions.find({articleId}, {sort: {startsAt: -1, endsAt: -1}});
});
Meteor.publish('currentPromotions', function () {
  const now = new Date();
  return Promotions.find({
    startsAt: { $lte: now },
    endsAt: { $gte: now },
  }, {sort: {startsAt: -1, endsAt: -1}});
});
