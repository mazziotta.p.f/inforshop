
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Promotions } from '/imports/api/promotions/promotions.js';
import { Articles } from '/imports/api/articles/articles.js';

const MAXIMUM_ARTICLE_ORDER_QUANTITY = 50;

Meteor.methods({
  'promotions.create' (articleId, discount, startsAt, endsAt) {

    if (this.connection) Security.check(['admin', 'manager', 'commercial']);

    check(articleId, String);
    check(startsAt, Date);
    check(endsAt, Date);
    check(discount, Number);

    const article = Articles.findOne({_id: articleId});

    if (!article)
      throw new Meteor.Error(404, 'Error 404: Article could not be found');

    Promotions.insert({
      articleId,
      discount,
      startsAt,
      endsAt,
    });
  },
  'promotions.remove'(promotionId) {

    if (this.connection) Security.check(['admin', 'manager', 'commercial']);

    check(promotionId, String);

    Promotions.remove({_id: promotionId});
  },
  'promotions.getArticlesWithBestDiscount'() {
    const now = new Date();
    let articlesWithBestDiscount = [];
    let articles = {};
    let promotions = [];
    Promotions.find({
      startsAt: { $lte: now },
      endsAt: { $gte: now },
    }).forEach(promotion => {
      if (Articles.findOne({_id: promotion.articleId, isAvailable: true})) {
        if (!articles.hasOwnProperty(promotion.articleId))
          articles[promotion.articleId] = [];
        articles[promotion.articleId].push(promotion);
      }
    });
    Object.keys(articles).forEach(key => {
      promotions.push({
        articleId: key,
        promotion: Calculator.calculateCumulativeDiscount(articles[key]),
      })
    });
    promotions.sort((a, b) => b.promotion - a.promotion)
      .slice(0, 3)
      .forEach(promotion => {
        let article = Articles.findOne(
          {_id: promotion.articleId},
          {fields: {imageName: 1, slug: 1, name: 1, price: 1, brandId: 1, categoryId: 1}}
        );
        if (article) articlesWithBestDiscount.push(article);
      });

    return articlesWithBestDiscount;
  }
});
