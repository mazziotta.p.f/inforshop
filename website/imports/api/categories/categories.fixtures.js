import { Categories } from './categories.js';

Meteor.startup(() => {
  if (Categories.find().count() === 0) {
    Meteor.call('categories.create', 'PC portable');
    Meteor.call('categories.addBindableValue', Categories.findOne({name: 'PC portable'})._id, 'pc portable');
    Meteor.call('categories.create', 'Écran');
    Meteor.call('categories.addBindableValue', Categories.findOne({name: 'Écran'})._id, 'écran');
    Meteor.call('categories.create', 'PC fixe');
    Meteor.call('categories.addBindableValue', Categories.findOne({name: 'PC fixe'})._id, 'pc fixe');
    Meteor.call('categories.create', 'Combo clavier souris');
    Meteor.call('categories.addBindableValue', Categories.findOne({name: 'Combo clavier souris'})._id, 'clavier ; souris');
  }
});
