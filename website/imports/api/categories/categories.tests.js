import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { Categories } from '/imports/api/categories/categories.js';
import { Articles } from '/imports/api/articles/articles.js';

import '/imports/api/categories/categories.methods.js';

if (Meteor.isServer) {
  describe('Categories', () => {
    describe('methods', () => {
      let name = "testCategory";
      let catId;

      beforeEach(() => {
        Articles.insert({
          name : "azerty",
          description : "qsdfgh",
          modelNumber : "wxcvbn",
          categoryName : "poiuy",
          price : 1,
          supplierDiscount : 0,
          brandId : "aaa",
        });

        Categories.remove({});
        catId = Categories.insert({
          slug: slugify("CategoryTest"),
          name: "CategoryTest",
          bindableValues: ["Category Test", "bindableToBeRemoved"]
        });
      });
      afterEach(() => {
        Articles.remove({});
        Categories.remove({});
      });
      it('can create a category', () =>{
        const addCat = Meteor.server.method_handlers['categories.create'];

        const invocation = {};

        addCat.apply(invocation, [name]);

        assert.equal(Categories.find().count(), 2);
      });
      it('can remove category', () => {
        const removeCat = Meteor.server.method_handlers['categories.remove'];

        const invocation = {};

        removeCat.apply(invocation, [catId]);

        assert.equal(Categories.find().count(), 0);
      });
      it('can add bindable value', () => {
        const addCatBindableValue = Meteor.server.method_handlers['categories.addBindableValue'];

        const invocation = {};

        addCatBindableValue.apply(invocation, [catId, "bazard"]);

        assert.equal(Categories.find({_id: catId, bindableValues: "bazard"}).count(), 1);
      });
      it('can remove bindable value', () => {
        const removeCatBindableValue = Meteor.server.method_handlers['categories.removeBindableValue'];

        const invocation = {};

        assert.equal(Categories.find({_id: catId, bindableValues: "bindableToBeRemoved"}).count(), 1);
        removeCatBindableValue.apply(invocation, [catId, "bindableToBeRemoved"]);
        assert.equal(Categories.find({_id: catId, bindableValues: "bindableToBeRemoved"}).count(), 0);
      });
    });
  });
}
