
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Categories } from '/imports/api/categories/categories.js';
import { Articles } from '/imports/api/articles/articles.js';
import { ArticleDefaults } from '/imports/api/articles/articleDefaults.js';

import { articleDefaultsBlueprint } from '/imports/blueprints/articleDefaults.blueprint.js';

Meteor.methods({
  'categories.create'(name) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(name, String);

    let articleDefaults = articleDefaultsBlueprint;
    articleDefaults.categoryId = Categories.insert({
      slug: slugify(name),
      name,
      bindableValues: [name]
    });
    ArticleDefaults.insert(articleDefaults);
  },
  'categories.remove'(categoryId) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(categoryId, String);

    if (Articles.find({categoryId}).count()) {
       throw new Meteor.Error('category-not-empty');
    }

    Categories.remove({_id: categoryId});
    ArticleDefaults.remove({categoryId});
  },


  // Add values inside array
  'categories.addBindableValue'(categoryId, value) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(categoryId, String);
    check(value, String);

    Categories.update(
      {_id: categoryId},
      {$push: {bindableValues: value}}
    );
  },

  // Remove value from array
  'categories.removeBindableValue'(categoryId, value) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(categoryId, String);
    check(value, String);

    Categories.update(
      {_id: categoryId},
      {$pull: {bindableValues: value}}
    );
  },
});
