
import { Meteor } from 'meteor/meteor';

import { ArticleTechnicals } from '/imports/api/articles/articleTechnicals.js';

Meteor.publish('articleTechnicals', () => ArticleTechnicals.find());
