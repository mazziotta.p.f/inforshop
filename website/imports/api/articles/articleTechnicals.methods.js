
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { ArticleTechnicals } from '/imports/api/articles/articleTechnicals.js';

Meteor.methods({
  'articleTechnicals.create'(name) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(name, String);

    ArticleTechnicals.insert({name});
  },
  'articleTechnicals.remove'(technicalId) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(technicalId, String);

    ArticleTechnicals.remove(technicalId);
  },
});
