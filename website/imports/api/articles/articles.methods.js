
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Articles } from '/imports/api/articles/articles.js';
import { Categories } from '/imports/api/categories/categories.js';
import { Brands } from '/imports/api/brands/brands.js';
import { GeneralConfiguration } from '/imports/api/generalConfiguration/generalConfiguration.js';

let articleWithMarginsAggragationQuery = [{
  $lookup: {
    from: 'salesMargins',
    localField: 'categoryId',
    foreignField: 'categoryId',
    as: 'categorySalesMargin'
  }
},
{
  $unwind: {
    path: '$categorySalesMargin',
    preserveNullAndEmptyArrays: true,
  }
},
{
  $project: {
    _id: 1,
    supplierDiscount: 1,
    advisedPrice: 1,
    categorySalesMargin: 1,
    brandMargin: {
      $filter:{
        input: '$categorySalesMargin.brandMargins',
        as: 'brandMargin',
        cond: {
          $eq: ['$$brandMargin.brandId', '$brandId']
        }
      }
    },
  }
}];

Meteor.methods({
  'articles.create'(name, description, modelNumber, categoryName, price, supplierDiscount) {

    if (this.connection) Security.check(['admin', 'manager', 'commercial']);

    check(name, String);
    check(description, String);
    check(modelNumber, String);
    check(categoryName, String);
    check(price, Number);
    check(supplierDiscount, Number);

    Articles.insert({
      name,
      description,
      modelNumber,
      categoryName,
      price,
      supplierDiscount,
    });
  },
  'articles.remove'(articleId) {

    if (this.connection) Security.check(['admin', 'manager', 'commercial']);

    check(articleId, String);

    Articles.remove({_id: articleId});
  },
  'articles.setAvailability'(articleId, isAvailable) {

    if (this.connection) Security.check(['admin', 'manager', 'commercial']);

    check(articleId, String);

    Articles.update(
      {_id: articleId},
      {$set: {isAvailable}}
    );
  },
  'articles.updateStringProperty'(articleId, name, value) {

    if (this.connection) Security.check(['admin', 'manager', 'commercial']);

    check(value, String);

    let updateObject = {};
    updateObject[name] = value;

    Articles.update({_id: articleId}, {$set: updateObject});

    if (name == 'brandId' || name == 'categoryId' || name == 'modelNumber' || name == 'series' || name == 'color') {
      updateNameAndSlug(articleId);
    }
    Meteor.call('articles.recalculatePrice', articleId);
  },
  'articles.updateNumberProperty'(articleId, name, value) {

    if (this.connection) Security.check(['admin', 'manager', 'commercial']);

    check(value, Number);

    if (isNaN(value)) {
      throw new Meteor.Error('Could not parse numbers !');
    }

    let updateObject = {};
    updateObject[name] = value;

    Articles.update({_id: articleId}, {$set: updateObject});
    Meteor.call('articles.recalculatePrice', articleId);
  },
  'articles.updateTechnicalProperty'(articleId, name, value) {

    if (this.connection) Security.check(['admin', 'manager', 'commercial']);

    check(name, String);
    check(value, String);

    Articles.update(
      {_id: articleId, 'technicals.name': name},
      {$set: {'technicals.$.value': value}}
    );
  },
  'articles.recalculatePrices'() {

    if (this.connection) Security.check(['admin', 'manager', 'commercial']);

    Articles.aggregate(articleWithMarginsAggragationQuery)
      .forEach(computeAndUpdateArticlePrice);
  },
  'articles.recalculatePrice'(articleId) {

    if (this.connection) Security.check(['admin', 'manager', 'commercial']);

    check(articleId, String);

    let query = articleWithMarginsAggragationQuery.map(a => Object.assign({}, a));

    query.unshift({
      $match: {
        _id: articleId,
      }
    });

    Articles.aggregate(query).forEach(computeAndUpdateArticlePrice);
  },
});

function computeAndUpdateArticlePrice(article) {
  let config = GeneralConfiguration.findOne({country: 'Belgium'});
  let price = article.advisedPrice;

  if (article.supplierDiscount) {
    price = price * (1 - (article.supplierDiscount/100));
  }
  if (article.brandMargin && article.brandMargin.length) {
    price = price * (1 + (article.brandMargin[0].margin/100));
  } else {
    if (article.categorySalesMargin) {
      price = price * (1 + (article.categorySalesMargin.defaultMargin/100));
    } else {
      price = price * (1 + (config.defaultMargin/100));
    }
  }

  price = parseFloat((price * (1 + (config.vat/100))).toFixed(2));

  Articles.update({_id: article._id}, {$set: {price}});
}

function updateNameAndSlug(articleId) {
  const article = Articles.findOne({_id: articleId});

  if (article) {
    const category = Categories.findOne({_id: article.categoryId});
    const brand = Brands.findOne({_id: article.brandId});

    if (category && brand) {
      name = createArticleName(article, category.name, brand.name);
      slug = slugify(name);

      Articles.update({_id: articleId}, {$set: {name, slug}})
    }
  }
}

function createArticleName(article, categoryName, brandName) {
  let name = categoryName + ' ' + brandName + ' ' + article.modelNumber;
  if (article.series) {
    name += ' ' + article.series;
  }
  if (article.color) {
    name += ' ' + article.color;
  }
  return name;
}
