import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { Brands } from '/imports/api/brands/brands.js';
import { Articles } from '/imports/api/articles/articles.js';
import { GeneralConfiguration } from '/imports/api/generalConfiguration/generalConfiguration.js';

import '/imports/api/articles/articles.methods.js';

if (Meteor.isServer) {
  describe('Articles', () => {
    describe('methods', () => {
      let articleId;

      beforeEach(() => {
        Articles.remove({});

        articleId = Articles.insert({
          name : "azerty",
          description : "qsdfgh",
          modelNumber : "wxcvbn",
          categoryName : "poiuy",
          price : 1,
          advisedPrice: 2,
          supplierDiscount : 0,
          brandId : "aaa",
          isAvailable: true,
          restockingTime: 42,
          color: "Yellow",
          technicals: [
            {
            name : "GPU",
			      value : "Intel"
            },
          ],
        });
        GeneralConfiguration.insert({
          country : "Belgium",
	        vat : 21,
          defaultMargin : 10,
          street : "Rue St Laurent 29",
          postCode : "4000",
          city : "Liège",
          deliveryFees: {},
        });
      });
      afterEach(() => {
        Articles.remove({});
      });

      it('can create article', () => {
        let name = "testArticle";
        let description = "Description of the article";
        let modelNumber = "Model Number 123456";
        let categoryName = "dummyCategory";
        let color = "bleu";
        let price = 99999;
        let supplierDiscount = 11111;

        const addArticle = Meteor.server.method_handlers['articles.create'];

        const invocation = {};

        addArticle.apply(invocation, [name, description, modelNumber, categoryName, price, supplierDiscount]);

        assert.equal(Articles.find().count(), 2);
      });
      it('can remove article', () => {
        const removeArticle = Meteor.server.method_handlers['articles.remove'];

        const invocation = {};

        removeArticle.apply(invocation, [articleId]);

        assert.equal(Articles.find().count(), 0);
      });
      it('can set availability', () => {
        let availability = false;

        const setArticleAvailabylity = Meteor.server.method_handlers['articles.setAvailability'];

        const invocation = {};

        setArticleAvailabylity.apply(invocation, [articleId, availability]);

        assert.equal(Articles.find({_id: articleId, isAvailable: false}).count(), 1);
      });
      it('can upgrade string property', () => {
        let name = "color";
        let value = "purple";

        const updateArticleStringProperty = Meteor.server.method_handlers['articles.updateStringProperty'];

        const invocation = {};

        updateArticleStringProperty.apply(invocation, [articleId, name, value]);

        assert.equal(Articles.find({_id: articleId, color: "purple"}).count(), 1);

      });
      it('can update number property', () => {
        let name = "restockingTime";
        let value = 555;

        const updateArticleNumberProperty = Meteor.server.method_handlers['articles.updateNumberProperty'];

        const invocation = {};

        updateArticleNumberProperty.apply(invocation, [articleId, name, value]);

        assert.equal(Articles.find({_id : articleId, restockingTime : 555}).count(), 1);
      });
      it('can update technical property', () => {
        let name = "GPU";
        let value = "ADM";

        const updateArticleTechnicalProperty = Meteor.server.method_handlers['articles.updateTechnicalProperty'];

        const invocation = {};

        updateArticleTechnicalProperty.apply(invocation, [articleId, name, value]);

        assert.equal(Articles.find({_id: articleId, technicals: {$elemMatch: {name, value}}}).count(), 1);
      });
      it('can update price', () => {
        const updateArticlePrice = Meteor.server.method_handlers['articles.recalculatePrice'];

        const invocation = {};

        updateArticlePrice.apply("", [articleId]);

        assert.equal(Articles.find({_id: articleId, price: 2.66}).count(), 1)
      });
    });
  });
}
