
import { Mongo } from 'meteor/mongo';

import { Index, MinimongoEngine } from 'meteor/easy:search'

export const Articles = new Mongo.Collection('articles');
export const ArticlesIndex = new Index({
  collection: Articles,
  fields: ['name', 'slug'],
  engine: new MinimongoEngine(),
  defaultSearchOptions: {
    limit: 25,
  }
});
