
import { Meteor } from 'meteor/meteor';

import { ArticleDefaults } from '/imports/api/articles/articleDefaults.js';

Meteor.publish('articleDefaults', function () {
  if (Security.isAllowed(['admin', 'manager']))
    return ArticleDefaults.find();
});
