
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { ArticleDefaults } from '/imports/api/articles/articleDefaults.js';

Meteor.methods({
  'articleDefaults.updateTechnicalProperty'(categoryId, name, value) {
    if (this.connection) Security.check(['admin', 'manager']);

    check(categoryId, String);
    check(name, String);
    check(value, String);

    const articleDefaults = ArticleDefaults.findOne({categoryId});

    if (!articleDefaults) throw new Meteor.Error('could-not-find-article-defaults');

    ArticleDefaults.update(
      {categoryId, 'technicals.name': name},
      {$set: {'technicals.$.value': value}}
    );
  },
  'articleDefaults.addTechnicalProperty'(categoryId, name, value) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(categoryId, String);
    check(name, String);
    check(value, String);

    const articleDefaults = ArticleDefaults.findOne({categoryId});

    if (!articleDefaults) throw new Meteor.Error('could-not-find-article-defaults');

    ArticleDefaults.update(
      {categoryId},
      {$push: {technicals: {name, value}}}
    );
  },
  'articleDefaults.removeTechnicalProperty'(categoryId, name) {

    if (this.connection) Security.check(['admin', 'manager']);

    check(categoryId, String);
    check(name, String);

    const articleDefaults = ArticleDefaults.findOne({categoryId});

    if (!articleDefaults) throw new Meteor.Error('could-not-find-article-defaults');

    ArticleDefaults.update(
      {categoryId},
      {$pull: {technicals: {name}}}
    );
  },
});
