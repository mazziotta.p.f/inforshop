
import { ArticleTechnicals } from './articleTechnicals.js';

Meteor.startup(() => {
  if (ArticleTechnicals.find().count() === 0) {
    Meteor.call('articleTechnicals.create', 'Garantie constructeur');
    Meteor.call('articleTechnicals.create', 'Système d\'exploitation');
    Meteor.call('articleTechnicals.create', 'Description du clavier');
    Meteor.call('articleTechnicals.create', 'Marque du processeur');
    Meteor.call('articleTechnicals.create', 'Type de processeur');
    Meteor.call('articleTechnicals.create', 'Vitesse du processeur');
    Meteor.call('articleTechnicals.create', 'Nombre de coeurs');
    Meteor.call('articleTechnicals.create', 'Taille de la mémoire vive');
    Meteor.call('articleTechnicals.create', 'Taille du disque dur');
    Meteor.call('articleTechnicals.create', 'Technologie du disque dur');
    Meteor.call('articleTechnicals.create', 'Type d\'écran');
    Meteor.call('articleTechnicals.create', 'Taille de l\'écran');
    Meteor.call('articleTechnicals.create', 'Résolution de l\'écran');
    Meteor.call('articleTechnicals.create', 'Résolution maximale d\'affichage');
    Meteor.call('articleTechnicals.create', 'Description de la carte graphique');
    Meteor.call('articleTechnicals.create', 'GPU');
    Meteor.call('articleTechnicals.create', 'GPU Ram');
    Meteor.call('articleTechnicals.create', 'Type de mémoire vive (carte graphique)');
    Meteor.call('articleTechnicals.create', 'Type de connectivité');
    Meteor.call('articleTechnicals.create', 'Bluetooth');
    Meteor.call('articleTechnicals.create', 'Nombre de ports HDMI');
    Meteor.call('articleTechnicals.create', 'Nombre de ports VGA');
    Meteor.call('articleTechnicals.create', 'Nombre de ports USB 2.0');
    Meteor.call('articleTechnicals.create', 'Nombre de ports USB 3.0');
    Meteor.call('articleTechnicals.create', 'Nombre de ports Ethernet');
    Meteor.call('articleTechnicals.create', 'Type de connecteur');
    Meteor.call('articleTechnicals.create', 'Item dimensions L x W x H');
    Meteor.call('articleTechnicals.create', 'Poids du produit');
    Meteor.call('articleTechnicals.create', 'Longueur du cable');
  }
});
