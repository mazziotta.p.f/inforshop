
import { Meteor } from 'meteor/meteor';

import { Articles } from '/imports/api/articles/articles.js';
import { Promotions } from '/imports/api/promotions/promotions.js';

Meteor.publish('articles', () => {
  if (Security.isAllowed(['admin', 'manager', 'commercial']))
    return Articles.find();
});
Meteor.publish('lightArticles', () => Articles.find({isAvailable: true},
  {fields: {imageName: 1, slug: 1, name: 1, price: 1, brandId: 1, categoryId: 1}}));
Meteor.publish('articleBySlug', (slug) => Articles.find({slug},
  {fields: {advisedPrice: 0, supplierDiscount: 0, stock: 0}}));
Meteor.publish('articleById', (articleId) => Articles.find({_id: articleId},
  {fields: {advisedPrice: 0, supplierDiscount: 0, stock: 0}}));
Meteor.publish('articlesResupply', () => Articles.find({},
  {fields: {name: 1, stock: 1, isAvailable: 1}}));
