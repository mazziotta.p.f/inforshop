
import { Meteor } from 'meteor/meteor';

import { Orders } from '/imports/api/orders/orders.js';

Meteor.publish('orders', function () {
  if (Security.isAllowed(['admin', 'manager', 'commercial']))
    return Orders.find({paidAt: {$ne: null}});
});
Meteor.publish('currentOrders', function () {
  if (Security.isConnected())
    return Orders.find({owner: Meteor.userId()});
});
