
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Carts } from '/imports/api/carts/carts.js';
import { Orders } from '/imports/api/orders/orders.js';
import { Articles } from '/imports/api/articles/articles.js';
import { GeneralConfiguration } from '/imports/api/generalConfiguration/generalConfiguration.js';

const MAXIMUM_ARTICLE_ORDER_QUANTITY = 50;

Meteor.methods({
  'orders.create'() {
    Security.denyIfNotConnected();

    const cart = Carts.findOne({owner: Meteor.userId});

    if (!cart || !cart.entries.length)
      throw new Meteor.Error(404, 'Error 404: no cart entries found');

    const order = buildOrder(cart);

    if (!order.entries.length)
      throw new Meteor.Error(500, 'Error 500: no article could be imported from cart to order');

    Orders.remove({owner: Meteor.userId, paidAt: null});
    const orderId = Orders.insert(order);

    return orderId;
  },
  'order.updateAddress'(orderId, address) {
    Security.denyIfNotConnected();

    check(orderId, String);
    check(address, String);

    Orders.update({_id: orderId}, {$set: {address}});
  },
  'order.updateDeliveryFee'(orderId, distance) {
    Security.denyIfNotConnected();

    check(orderId, String);
    check(distance, Number);

    const config = GeneralConfiguration.findOne();

    if (!config)
      throw new Meteor.Error(404, 'Error 404: no config found');

    const order = Orders.findOne({_id: orderId});

    if (!order)
      throw new Meteor.Error(404, 'Error 404: order not found');

    if (order.owner !== Meteor.userId())
      throw new Meteor.Error(403, 'Error 403: unauthorized access to order');

    let foundFee = {minimumDistance: -1};
    config.deliveryFees.forEach(fee => {
      if (fee.minimumDistance <= distance && fee.minimumDistance > foundFee.minimumDistance)
        foundFee = fee;
    });

    if (foundFee.minimumDistance < 0)
      throw new Meteor.Error(404, 'Error 404: no fee for delivery found');

    let deliveryFee = parseFloat(foundFee.fee);

    Orders.update(
      {_id: orderId},
      {$set: {
        deliveryFee,
        totalPrice: order.totalArticlesPrice + deliveryFee,
        deliveryFeeSetAt: new Date()
      }}
    );
  },
  'order.validate'(orderId) {
    check(orderId, String);

    const order = Orders.findOne({_id: orderId});

    if (!order)
      throw new Meteor.Error(404, 'Error 404: order not found');

    if (order.owner !== Meteor.userId())
      throw new Meteor.Error(403, 'Error 403: unauthorized access to order');

    if (!order.deliveryFeeSetAt) return false;

    Orders.update(
      {_id: orderId},
      {$set: {
        validatedAt: new Date(),
        paymentInformationSetAt: null
      }}
    );
    return true;
  },
  'order.paymentInformationIsSet'(orderId) {
    check(orderId, String);

    const order = Orders.findOne({_id: orderId});

    if (!order)
      throw new Meteor.Error(404, 'Error 404: order not found');

    if (order.owner !== Meteor.userId())
      throw new Meteor.Error(403, 'Error 403: unauthorized access to order');

    if (!order.validatedAt) return false;

    Orders.update(
      {_id: orderId},
      {$set: {
        paymentInformationSetAt: new Date()
      }}
    );
    return true;
  },
  'order.paymentInformationIsUnset'(orderId) {
    check(orderId, String);

    const order = Orders.findOne({_id: orderId});

    if (!order)
      throw new Meteor.Error(404, 'Error 404: order not found');

    if (order.owner !== Meteor.userId())
      throw new Meteor.Error(403, 'Error 403: unauthorized access to order');

    if (!order.validatedAt) return false;

    Orders.update(
      {_id: orderId},
      {$set: {
        paymentInformationSetAt: null
      }}
    );
    return true;
  },
  'order.pay'(orderId) {
    check(orderId, String);

    const order = Orders.findOne({_id: orderId});

    if (!order)
      throw new Meteor.Error(404, 'Error 404: order not found');

    if (order.owner !== Meteor.userId())
      throw new Meteor.Error(403, 'Error 403: unauthorized access to order');

    if (!order.paymentInformationSetAt) return false;

    Orders.update(
      {_id: orderId},
      {$set: {
        paidAt: new Date()
      }}
    );
    return true;
  },
  'order.getMostOrderedArticles'() {
    const articles = Orders.aggregate([
      { $match: {paidAt: {$ne: null}} },
      { $unwind: '$entries' },
      {
        $group: {
          _id: '$entries._id',
          quantity: {$sum: '$entries.quantity'}
        }
      },
      { $sort: { quantity: -1 } },
      { $limit: 3 }
    ]);

    if (!articles) return [];

    const articleIds = articles.map(article => article._id);

    return Articles.find({_id: {$in: articleIds} },
      {fields: {imageName: 1, slug: 1, name: 1, price: 1, brandId: 1, categoryId: 1}}
    ).fetch();
  },
  'order.prepare'(orderId) {
    if (this.connection) Security.check(['admin', 'manager', 'commercial']);

    check(orderId, String);

    const order = Orders.findOne({_id: orderId});

    if (!order)
      throw new Meteor.Error(404, 'Error 404: order not found');

    if (!order.paidAt) return false;

    order.entries.forEach(entry => Articles.update({_id: entry._id}, {$inc: {stock: -entry.quantity}}));
    Orders.update(
      {_id: orderId},
      {$set: {
        preparedAt: new Date()
      }}
    );
    return true;
  },
  'order.send'(orderId) {
    if (this.connection) Security.check(['admin', 'manager', 'commercial']);

    check(orderId, String);

    const order = Orders.findOne({_id: orderId});

    if (!order)
      throw new Meteor.Error(404, 'Error 404: order not found');

    if (!order.preparedAt) return false;

    Orders.update(
      {_id: orderId},
      {$set: {
        sentAt: new Date()
      }}
    );
    return true;
  },
});

function buildOrder(cart) {
  let order = {};

  order.owner = Meteor.userId();
  order.createdAt = new Date();
  order.deliveryFeeSetAt = null;
  order.paymentInformationSetAt = null;
  order.validatedAt = null;
  order.paidAt = null;
  order.preparedAt = null;
  order.sentAt = null;
  order.entries = [];
  order.totalArticlesPrice = 0;
  cart.entries.forEach(entry => {
    const article = Articles.findOne(
      {_id: entry.articleId, isAvailable: true},
      {fields: {advisedPrice: 0, supplierDiscount: 0, stock: 0}}
    );
    if (article) {
      article.quantity = entry.quantity;
      article.promotions = Calculator.articleDiscounts(article);
      if (article.promotions.length) {
        article.discount = Calculator.calculateCumulativeDiscount(article.promotions);
        article.discountedPrice = Calculator.calculateDiscountedPrice(article, article.discount);
        order.totalArticlesPrice += article.discountedPrice * article.quantity;
      } else {
        order.totalArticlesPrice += article.price * article.quantity;
      }
      order.totalPrice = order.totalArticlesPrice;
      order.entries.push(article);
    }
  });

  return order;
}
