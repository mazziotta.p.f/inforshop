
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { SalesMargins } from '/imports/api/salesMargins/salesMargins.js';

Meteor.methods({
  'salesMarginsCategory.create' (categoryId, categoryName, value){

    if (this.connection) Security.check(['admin', 'commercial']);

    check(categoryId, String);
    check(categoryName, String);
    check(value, Number);

    if (!SalesMargins.findOne({categoryId})) {
      SalesMargins.insert({
        categoryId,
        categoryName,
        defaultMargin: value,
        brandMargins: [],
      });
      Meteor.call('articles.recalculatePrices');
    }
    else {
      throw new Meteor.Error('Category already created');
    }
  },
  'salesMarginsCategory.update'(salesMarginId, value){

    if (this.connection) Security.check(['admin', 'commercial']);

    check(salesMarginId, String);
    check(value, Number);

    if (SalesMargins.findOne({_id: salesMarginId})) {
      SalesMargins.update(
        {_id: salesMarginId},
        {$set: {defaultMargin: value}},
      );
      Meteor.call('articles.recalculatePrices');
    }
    else {
      throw new Meteor.Error('No sales margins ID');
    }
  },
  'salesMarginsCategory.remove'(salesMarginId){

    if (this.connection) Security.check(['admin', 'commercial']);

    check(salesMarginId, String);

    if (SalesMargins.findOne({_id: salesMarginId})) {
      SalesMargins.remove({
        _id: salesMarginId,
      });
      Meteor.call('articles.recalculatePrices');
    }
    else {
      throw new Meteor.Error('No sales margins ID');
    }
  },
  'salesMarginsBrand.create'(salesMarginId, brandId, brandName, value){

    if (this.connection) Security.check(['admin', 'commercial']);

    check(salesMarginId, String);
    check(brandId, String);
    check(brandName, String);
    check(value, Number);

    if (SalesMargins.findOne({_id: salesMarginId})) {
      // Create if brandId doesn't exists
      SalesMargins.update(
        {
          _id: salesMarginId,
          "brandMargins.brandId": {$ne: brandId}
        },
        {
          $addToSet: { brandMargins: {
            brandId: brandId,
            brandName: brandName,
            margin: value
          }}
        },
        false,
        true);
      }
    Meteor.call('articles.recalculatePrices');
  },
  'salesMarginsBrand.update'(salesMarginId, brandId, value){

    if (this.connection) Security.check(['admin', 'commercial']);

    check(salesMarginId, String);
    check(brandId, String);
    check(value, Number);

    if (SalesMargins.findOne({_id: salesMarginId})) {
      // Update if exists
      SalesMargins.update(
        {
          _id: salesMarginId,
          "brandMargins.brandId": brandId
        },
        {
          $set: {
            "brandMargins.$.brandId": brandId,
            "brandMargins.$.margin": value,
          }
      },
      false,
      true);
    Meteor.call('articles.recalculatePrices');
    }
  },
  'salesMarginsBrand.remove'(salesMarginId, brandId){

    if (this.connection) Security.check(['admin', 'commercial']);

    check(salesMarginId, String);
    check(brandId, String);

    if (SalesMargins.findOne({_id: salesMarginId})) {
      SalesMargins.update(
        {_id: salesMarginId},
        {
          $pull: {
            brandMargins: {brandId}
          }
      });
    }
    Meteor.call('articles.recalculatePrices');
  },
});
