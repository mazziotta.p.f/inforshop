
import { Meteor } from 'meteor/meteor';

import { SalesMargins } from '/imports/api/salesMargins/salesMargins.js';

Meteor.publish('salesMargins', function(){
  if (Security.isAllowed(['admin', 'manager', 'commercial']))
    return SalesMargins.find();
});
