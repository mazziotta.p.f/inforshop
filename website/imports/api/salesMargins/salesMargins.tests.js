import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { assert } from 'meteor/practicalmeteor:chai';

import { SalesMargins } from '/imports/api/salesMargins/salesMargins.js';

import '/imports/api/salesMargins/salesMargins.methods.js';

if (Meteor.isServer) {
  describe('SalesMargins', () => {
    describe('methods', () => {
      let salesMarginId;
      let categoryId = Random.id();
      let brandId = Random.id();

      beforeEach(() => {
        salesMarginId = SalesMargins.insert({
          categoryId,
          categoryName : "catTestName",
          defaultMargin: 22,
          brandMargins: [
            {
              brandId,
              brandName: "testBrandName",
              margin: 12
            }
          ],
        });
      });
      afterEach(() => {
        SalesMargins.remove({});
      });

      it('can create sales margins', () => {
        let newCatId = Random.id();
        let newCatName = "newCatNameTest";
        let margin = 55;

        const createSalesMargin = Meteor.server.method_handlers['salesMarginsCategory.create'];

        const invocation = {};

        createSalesMargin.apply(invocation, [newCatId, newCatName, margin]);

        assert.equal(SalesMargins.find({categoryId: newCatId}).count(), 1);
      });
      it('can update sales margins', () => {
        let newMargin = 30;
        const updateSalesMargins = Meteor.server.method_handlers['salesMarginsCategory.update'];

        const invocation = {};

        updateSalesMargins.apply(invocation, [salesMarginId, newMargin]);
        
        assert.equal(SalesMargins.find({categoryId, defaultMargin: 30}).count(), 1);
      });
      it('can remove sales margins', () => {
        const removeSalesMargins = Meteor.server.method_handlers['salesMarginsCategory.remove'];

        const invocation = {};

        assert.equal(SalesMargins.find({_id: salesMarginId}).count(), 1);
        removeSalesMargins.apply(invocation, [salesMarginId]);
        assert.equal(SalesMargins.find({_id: salesMarginId}).count(), 0);
      });
      it('can add brand to sales margins', () => {
        let brandId = Random.id();
        let brandName = "NewBrand";
        let margin = 33;

        const addBrandToSalesMargins = Meteor.server.method_handlers['salesMarginsBrand.create'];

        const invocation = {};

        addBrandToSalesMargins.apply(invocation, [salesMarginId, brandId, brandName, margin]);

        assert.equal(SalesMargins.find({_id: salesMarginId, brandMargins: {$elemMatch: {brandId, brandName, margin}}}).count(), 1);
      });
      it('can update brand in sales margins', () => {
        let newMargin = 23;

        const updateBrandInSalesMargins = Meteor.server.method_handlers['salesMarginsBrand.update'];

        const invocation = {};

        updateBrandInSalesMargins.apply(invocation, [salesMarginId, brandId, newMargin]);

        assert.equal(SalesMargins.find({_id: salesMarginId, brandMargins: {$elemMatch: {brandId, margin: newMargin}}}).count(), 1);
      });
      it('can remove brand in sales margins', () => {
        const removeBrandInSalesMargins = Meteor.server.method_handlers['salesMarginsBrand.remove'];

        const invocation = {};

        assert.equal(SalesMargins.find({_id: salesMarginId, brandMargins: {$elemMatch: {brandId}}}).count(), 1);
        removeBrandInSalesMargins.apply(invocation, [salesMarginId, brandId]);
        assert.equal(SalesMargins.find({_id: salesMarginId, brandMargins: {$elemMatch: {brandId}}}).count(), 0);
      });
    });
  });
}
