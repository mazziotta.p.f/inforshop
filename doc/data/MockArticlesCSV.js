export const MockArticlesCSV = [
  {
    "Image": "91bs2mx5xnL._SL1500_.jpg",
    "Prix conseillé HTVA (€)": "248",
    "Remise fournisseur (%)": "5",
    "Délai (jours)": "7",
    "Type": "pc portable",
    "Marque": "HP",
    "Numéro du modèle de l'article": "2CJ71EA#ABF",
    "séries": "15-bs011nf",
    "Couleur": "Noir ébène",
    "Garantie constructeur": "Garantie Fabricant: 1 an",
    "Longueur du cable": "",
    "Divers": "solut"
  },
  {
    "Image": "51BWTFS4P0L.jpg",
    "Prix conseillé HTVA (€)": "772,9",
    "Remise fournisseur (%)": "",
    "Délai (jours)": "8",
    "Type": "pc portable",
    "Marque": "Acer",
    "Numéro du modèle de l'article": "NX.GNVEF.008",
    "séries": "",
    "Couleur": "Noir",
    "Garantie constructeur": "Garantie Fabricant: 2 ans",
    "Longueur du cable": "",
    "Divers": ""
  }
];
