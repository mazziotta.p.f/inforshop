[
User: {},

Client: {
    userId,
    clientNumber: 'XXNNNNN',
    firstName,
    lastName,
    address,
    eMail,
    phoneNumber,
    createdAt: Date,
},

Cart: {
    owner: clientNumber,
    articles: [CartItem],
    deliveryPrice,
    isActive,
},

CartItem: {
    articleId,
    quantity,
},

// Builds from Cart
Order: {
    owner: clientNumber,
    items: [OrderItem],
    deliveryPrice,
    status: inPreparation/inTransit/delivered ,
    isPaid,
    isPaidWithCreditCard,
    // if createdAt one hour ago, quantity is not decremented from stock
    createdAt: Date,
},

OrderItem: {
    article: Article // copy of actual article,
    quantity,
},

// Order copy at payment
Invoice: {
    orderId,
    createdAt: Date,
},

Article: {
    name,
    description,
    slug,
    price,  
    categoryId,
    // For inventory purpose, substitute to a state indicating availability
    _quantity,
    isAvailable,
    createdAt: Date,
},

ArticleTechnicals: {
    articleId,
    name,
    content,
},

Category: {
    name,
    _parent,    
},

Discount: {
    appliesTo: articleId or categoryId,
    percentage,
    startsAt: Date,
    endsAt: Date,   
},

Stock,  // virtual view of articles taking orders.status and createdAt
        // into account

Inventory: {
    articles: [Article], // copy of articles
},

Supplier: {
    name,    
},

// Mostly operationnal, csv parsed into json
Resupply: {
    supplierId,
},

ResupplyLine: {
    resupplyId,
    article,
    quantity,
},

]

